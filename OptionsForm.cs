/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RemObjects.Hydra.CrossPlatform;
using tide.PluginInterfaces;
using RemObjects.Hydra;
using tide.Utilities.Forms;

namespace tide
{
    public partial class OptionsForm : XtraForm
    {
        private ModuleManager moduleManager;
        private IHYCrossPlatformHost pluginHost;
        private List<IOptionsPage> pluginList;

        #region Event Handlers
        
        private void OptionsForm_Load(object sender, EventArgs e)
        {
            using (new HourGlass())
            {
                LoadOptionsPages();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            using (new HourGlass())
            {
                SaveOptions();
            }
        }

        #endregion
        
        public OptionsForm(ModuleManager moduleManager, IHYCrossPlatformHost pluginHost)
        {
            InitializeComponent();
            pluginList = new List<IOptionsPage>();
            this.moduleManager = moduleManager;
            this.pluginHost = pluginHost;
        }

        private void handleCheckChanged(object sender, EventArgs e)
        {
            IOptionsPage page = (IOptionsPage)((CheckButton)sender).Tag;
            if (page is VisualPlugin)
            {
                VisualPlugin visualPlugin = (VisualPlugin)page;
                visualPlugin.Dock = DockStyle.Fill;

                hostPanel.UnhostPlugin();
                hostPanel.HostPlugin(visualPlugin);
            }
        }

        private void CreatePluginForDescriptor(PluginDescriptor plugin)
        {
            IHYCrossPlatformPlugin currentPlugin = moduleManager.CreateInstance(plugin);
            currentPlugin.Host = pluginHost;

            IOptionsPage page = currentPlugin as IOptionsPage;
            pluginList.Add(page);
            CheckButton checkButton = new CheckButton { Text = page.Caption(), Tag = page, Dock = DockStyle.Top, 
                Height = 26, Parent = buttonsPanel, AllowAllUnchecked = false, GroupIndex = 1, Cursor = Cursors.Arrow };
            checkButton.CheckedChanged += handleCheckChanged;
            page.LoadOptions();
        }

        private void CheckFirstOptionsButton()
        {
            if (buttonsPanel.Controls.Count > 0)
            {
                CheckButton checkButton = (CheckButton)buttonsPanel.Controls[buttonsPanel.Controls.Count - 1];
                checkButton.Checked = true;
            }
        }

        private void LoadOptionsPages()
        {
            /* we can't use the plugin instance that is in DataModule.ObjectInstance.Plugins()
             * this is because the plugin will be disposed when the HostPanel is destroyed
             * which causes an error the second time you show options */
            foreach (PluginDescriptor plugin in moduleManager.Plugins)
                if (plugin.CheckPluginInterface(typeof(IOptionsPage)))
                    CreatePluginForDescriptor(plugin);
            CheckFirstOptionsButton();
        }

        private void SaveOptions()
        {
            foreach (IOptionsPage page in pluginList)
                if (!page.SaveOptions())
                {
                    if (page is IVisualPlugin)
                        hostPanel.HostPlugin((IVisualPlugin)page);

                    return;
                }
            DialogResult = DialogResult.OK;
        }
    }
}
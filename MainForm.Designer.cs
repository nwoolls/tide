namespace tide
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.DefaultToolTipController defaultToolTipController;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.bottomControlContainer = new DevExpress.XtraBars.PopupControlContainer();
            this.exitButton = new DevExpress.XtraEditors.SimpleButton();
            this.optionsButton = new DevExpress.XtraEditors.SimpleButton();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu = new DevExpress.XtraBars.Ribbon.ApplicationMenu();
            this.newItemMenu = new DevExpress.XtraBars.BarSubItem();
            this.saveItemMenu = new DevExpress.XtraBars.BarSubItem();
            this.printMenu = new DevExpress.XtraBars.BarSubItem();
            this.printButton = new DevExpress.XtraBars.BarButtonItem();
            this.quickPrintButton = new DevExpress.XtraBars.BarButtonItem();
            this.printPreviewButton = new DevExpress.XtraBars.BarButtonItem();
            this.exportMenu = new DevExpress.XtraBars.BarSubItem();
            this.htmlExportButton = new DevExpress.XtraBars.BarButtonItem();
            this.pdfExportButton = new DevExpress.XtraBars.BarButtonItem();
            this.xlsExportButton = new DevExpress.XtraBars.BarButtonItem();
            this.otherExportButton = new DevExpress.XtraBars.BarButtonItem();
            this.skinsSubItem = new DevExpress.XtraBars.BarSubItem();
            this.appMenuFooterContainer = new DevExpress.XtraBars.BarLinkContainerItem();
            this.newItemButton = new DevExpress.XtraBars.BarButtonItem();
            this.saveItemButton = new DevExpress.XtraBars.BarButtonItem();
            this.deleteItemButton = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.barController = new DevExpress.XtraBars.DefaultBarAndDockingController();
            this.navBarControl = new DevExpress.XtraNavBar.NavBarControl();
            this.pluginTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager();
            this.sharedDictionaryStorage = new DevExpress.XtraSpellChecker.SharedDictionaryStorage();
            this.spellChecker = new DevExpress.XtraSpellChecker.SpellChecker();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.moduleManager = new RemObjects.Hydra.ModuleManager();
            this.lookAndFeel = new DevExpress.LookAndFeel.DefaultLookAndFeel();
            defaultToolTipController = new DevExpress.Utils.DefaultToolTipController();
            ((System.ComponentModel.ISupportInitialize)(this.bottomControlContainer)).BeginInit();
            this.bottomControlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pluginTabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // defaultToolTipController
            // 
            // 
            // 
            // 
            defaultToolTipController.DefaultController.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip;
            // 
            // bottomControlContainer
            // 
            defaultToolTipController.SetAllowHtmlText(this.bottomControlContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.bottomControlContainer.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.bottomControlContainer.Appearance.Options.UseBackColor = true;
            this.bottomControlContainer.AutoSize = true;
            this.bottomControlContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bottomControlContainer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.bottomControlContainer.Controls.Add(this.exitButton);
            this.bottomControlContainer.Controls.Add(this.optionsButton);
            this.bottomControlContainer.Location = new System.Drawing.Point(238, 236);
            this.bottomControlContainer.Name = "bottomControlContainer";
            this.bottomControlContainer.Ribbon = this.ribbonControl;
            this.bottomControlContainer.Size = new System.Drawing.Size(155, 30);
            this.bottomControlContainer.TabIndex = 12;
            this.bottomControlContainer.Visible = false;
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(77, 4);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 1;
            this.exitButton.Text = "Exit";
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // optionsButton
            // 
            this.optionsButton.Location = new System.Drawing.Point(0, 4);
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.Size = new System.Drawing.Size(75, 23);
            this.optionsButton.TabIndex = 0;
            this.optionsButton.Text = "Options";
            this.optionsButton.Click += new System.EventHandler(this.optionsButton_Click);
            // 
            // ribbonControl
            // 
            this.ribbonControl.ApplicationButtonDropDownControl = this.applicationMenu;
            this.ribbonControl.ApplicationButtonText = null;
            this.ribbonControl.ApplicationIcon = global::tide.Properties.Resources.cubes;
            // 
            // 
            // 
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.ExpandCollapseItem.Name = "";
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.skinsSubItem,
            this.appMenuFooterContainer,
            this.newItemMenu,
            this.saveItemMenu,
            this.newItemButton,
            this.saveItemButton,
            this.quickPrintButton,
            this.printPreviewButton,
            this.printMenu,
            this.exportMenu,
            this.htmlExportButton,
            this.pdfExportButton,
            this.xlsExportButton,
            this.otherExportButton,
            this.printButton,
            this.deleteItemButton});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 87;
            this.ribbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Never;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl.Size = new System.Drawing.Size(931, 54);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.ribbonControl.Toolbar.ItemLinks.Add(this.skinsSubItem);
            this.ribbonControl.Toolbar.ItemLinks.Add(this.newItemButton, true);
            this.ribbonControl.Toolbar.ItemLinks.Add(this.deleteItemButton);
            this.ribbonControl.Toolbar.ItemLinks.Add(this.saveItemButton);
            this.ribbonControl.Toolbar.ItemLinks.Add(this.printButton);
            this.ribbonControl.TransparentEditors = true;
            this.ribbonControl.ApplicationButtonDoubleClick += new System.EventHandler(this.ribbonControl_ApplicationButtonDoubleClick);
            // 
            // applicationMenu
            // 
            this.applicationMenu.BottomPaneControlContainer = this.bottomControlContainer;
            this.applicationMenu.ItemLinks.Add(this.newItemMenu);
            this.applicationMenu.ItemLinks.Add(this.saveItemMenu);
            this.applicationMenu.ItemLinks.Add(this.printMenu);
            this.applicationMenu.ItemLinks.Add(this.exportMenu);
            this.applicationMenu.Name = "applicationMenu";
            this.applicationMenu.Ribbon = this.ribbonControl;
            this.applicationMenu.ShowRightPane = true;
            // 
            // newItemMenu
            // 
            this.newItemMenu.Caption = "New";
            this.newItemMenu.Id = 14;
            this.newItemMenu.LargeGlyph = global::tide.Properties.Resources.documents_new;
            this.newItemMenu.Name = "newItemMenu";
            this.newItemMenu.Popup += new System.EventHandler(this.newItemMenu_Popup);
            // 
            // saveItemMenu
            // 
            this.saveItemMenu.Caption = "Save";
            this.saveItemMenu.Id = 68;
            this.saveItemMenu.LargeGlyph = global::tide.Properties.Resources.disks;
            this.saveItemMenu.Name = "saveItemMenu";
            this.saveItemMenu.Popup += new System.EventHandler(this.saveItemMenu_Popup);
            // 
            // printMenu
            // 
            this.printMenu.Caption = "Print";
            this.printMenu.Glyph = global::tide.Properties.Resources.printer1;
            this.printMenu.Id = 74;
            this.printMenu.LargeGlyph = global::tide.Properties.Resources.printer;
            this.printMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.quickPrintButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewButton)});
            this.printMenu.Name = "printMenu";
            // 
            // printButton
            // 
            this.printButton.Caption = "Print";
            this.printButton.Description = "Select a printer, number of copies, and other options before printing.";
            this.printButton.Glyph = global::tide.Properties.Resources.printer1;
            this.printButton.Hint = "Print";
            this.printButton.Id = 84;
            this.printButton.LargeGlyph = global::tide.Properties.Resources.printer;
            this.printButton.Name = "printButton";
            this.printButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printButton_ItemClick);
            // 
            // quickPrintButton
            // 
            this.quickPrintButton.Caption = "Quick Print";
            this.quickPrintButton.Description = "Send the document directly to the default printer without making changes.";
            this.quickPrintButton.Glyph = global::tide.Properties.Resources.printer1;
            this.quickPrintButton.Id = 71;
            this.quickPrintButton.LargeGlyph = global::tide.Properties.Resources.printer_ok;
            this.quickPrintButton.Name = "quickPrintButton";
            this.quickPrintButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // printPreviewButton
            // 
            this.printPreviewButton.Caption = "Print Preview";
            this.printPreviewButton.Description = "Preview and make changes to pages before printing.";
            this.printPreviewButton.Glyph = global::tide.Properties.Resources.printer_view1;
            this.printPreviewButton.Id = 72;
            this.printPreviewButton.LargeGlyph = global::tide.Properties.Resources.printer_view;
            this.printPreviewButton.Name = "printPreviewButton";
            this.printPreviewButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // exportMenu
            // 
            this.exportMenu.Caption = "Export";
            this.exportMenu.Id = 75;
            this.exportMenu.LargeGlyph = global::tide.Properties.Resources.document_into;
            this.exportMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, this.htmlExportButton, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "", ""),
            new DevExpress.XtraBars.LinkPersistInfo(this.pdfExportButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.xlsExportButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.otherExportButton)});
            this.exportMenu.Name = "exportMenu";
            // 
            // htmlExportButton
            // 
            this.htmlExportButton.Caption = "Webpage";
            this.htmlExportButton.Description = "Export to HTML";
            this.htmlExportButton.Id = 80;
            this.htmlExportButton.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("htmlExportButton.LargeGlyph")));
            this.htmlExportButton.Name = "htmlExportButton";
            this.htmlExportButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.htmlExportButton_ItemClick);
            // 
            // pdfExportButton
            // 
            this.pdfExportButton.Caption = "Acrobat";
            this.pdfExportButton.Description = "Export to PDF";
            this.pdfExportButton.Id = 81;
            this.pdfExportButton.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("pdfExportButton.LargeGlyph")));
            this.pdfExportButton.Name = "pdfExportButton";
            this.pdfExportButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.pdfExportButton_ItemClick);
            // 
            // xlsExportButton
            // 
            this.xlsExportButton.Caption = "Spreadsheet";
            this.xlsExportButton.Description = "Export to XLS";
            this.xlsExportButton.Id = 82;
            this.xlsExportButton.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("xlsExportButton.LargeGlyph")));
            this.xlsExportButton.Name = "xlsExportButton";
            this.xlsExportButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.xlsExportButton_ItemClick);
            // 
            // otherExportButton
            // 
            this.otherExportButton.Caption = "Other";
            this.otherExportButton.Description = "Export to another file format";
            this.otherExportButton.Id = 83;
            this.otherExportButton.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("otherExportButton.LargeGlyph")));
            this.otherExportButton.Name = "otherExportButton";
            this.otherExportButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.otherExportButton_ItemClick);
            // 
            // skinsSubItem
            // 
            this.skinsSubItem.Caption = "Skins";
            this.skinsSubItem.Description = "Description";
            this.skinsSubItem.Glyph = global::tide.Properties.Resources.window_colors;
            this.skinsSubItem.Hint = "Change the look and feel of the application";
            this.skinsSubItem.Id = 3;
            this.skinsSubItem.Name = "skinsSubItem";
            // 
            // appMenuFooterContainer
            // 
            this.appMenuFooterContainer.Caption = "Application Menu Footer Container";
            this.appMenuFooterContainer.Id = 6;
            this.appMenuFooterContainer.Name = "appMenuFooterContainer";
            // 
            // newItemButton
            // 
            this.newItemButton.Caption = "New";
            this.newItemButton.Glyph = global::tide.Properties.Resources.document_plain_new;
            this.newItemButton.Id = 69;
            this.newItemButton.LargeGlyph = global::tide.Properties.Resources.document_plain_new;
            this.newItemButton.Name = "newItemButton";
            this.newItemButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // saveItemButton
            // 
            this.saveItemButton.Caption = "Save";
            this.saveItemButton.Glyph = global::tide.Properties.Resources.disk_blue;
            this.saveItemButton.Id = 70;
            this.saveItemButton.Name = "saveItemButton";
            this.saveItemButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // deleteItemButton
            // 
            this.deleteItemButton.Caption = "Delete";
            this.deleteItemButton.Glyph = global::tide.Properties.Resources.delete2;
            this.deleteItemButton.Id = 85;
            this.deleteItemButton.Name = "deleteItemButton";
            this.deleteItemButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.UseParentBackground = true;
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.AllowFocused = false;
            this.repositoryItemZoomTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar1.Maximum = 180;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            this.repositoryItemZoomTrackBar1.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            this.repositoryItemZoomTrackBar1.UseParentBackground = true;
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 647);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(931, 23);
            // 
            // navBarControl
            // 
            this.navBarControl.ActiveGroup = null;
            this.navBarControl.ContentButtonHint = null;
            this.navBarControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarControl.Location = new System.Drawing.Point(0, 54);
            this.navBarControl.Name = "navBarControl";
            this.navBarControl.OptionsNavPane.ExpandedWidth = 230;
            this.navBarControl.Size = new System.Drawing.Size(230, 593);
            this.navBarControl.TabIndex = 5;
            this.navBarControl.Text = "navBarControl";
            this.navBarControl.View = new DevExpress.XtraNavBar.ViewInfo.SkinNavigationPaneViewInfoRegistrator();
            this.navBarControl.Visible = false;
            // 
            // pluginTabControl
            // 
            this.pluginTabControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pluginTabControl.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pluginTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pluginTabControl.Location = new System.Drawing.Point(230, 54);
            this.pluginTabControl.Name = "pluginTabControl";
            this.pluginTabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.pluginTabControl.Size = new System.Drawing.Size(701, 593);
            this.pluginTabControl.TabIndex = 8;
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // spellChecker
            // 
            this.spellChecker.Culture = new System.Globalization.CultureInfo("en-US");
            this.spellChecker.ParentContainer = null;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "document_plain_new.png");
            this.imageCollection1.Images.SetKeyName(1, "disk_blue.png");
            // 
            // moduleManager
            // 
            this.moduleManager.Host = null;
            // 
            // lookAndFeel
            // 
            this.lookAndFeel.LookAndFeel.SkinName = "Money Twins";
            // 
            // MainForm
            // 
            defaultToolTipController.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 670);
            this.Controls.Add(this.bottomControlContainer);
            this.Controls.Add(this.pluginTabControl);
            this.Controls.Add(this.navBarControl);
            this.Controls.Add(this.ribbonControl);
            this.Controls.Add(this.ribbonStatusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Ribbon = this.ribbonControl;
            this.StatusBar = this.ribbonStatusBar;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.bottomControlContainer)).EndInit();
            this.bottomControlContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pluginTabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.DefaultBarAndDockingController barController;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu;
        private DevExpress.XtraNavBar.NavBarControl navBarControl;
        private DevExpress.XtraBars.BarSubItem skinsSubItem;
        private DevExpress.XtraBars.BarLinkContainerItem appMenuFooterContainer;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraSpellChecker.SharedDictionaryStorage sharedDictionaryStorage;
        private DevExpress.XtraSpellChecker.SpellChecker spellChecker;
        private DevExpress.XtraBars.BarSubItem newItemMenu;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraBars.PopupControlContainer bottomControlContainer;
        private DevExpress.XtraEditors.SimpleButton exitButton;
        private DevExpress.XtraEditors.SimpleButton optionsButton;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarSubItem saveItemMenu;
        private DevExpress.XtraBars.BarButtonItem newItemButton;
        private DevExpress.XtraBars.BarButtonItem saveItemButton;
        private DevExpress.XtraBars.BarButtonItem quickPrintButton;
        private DevExpress.XtraBars.BarButtonItem printPreviewButton;
        private DevExpress.XtraBars.BarSubItem printMenu;
        private DevExpress.XtraBars.BarSubItem exportMenu;
        private DevExpress.XtraBars.BarButtonItem htmlExportButton;
        private DevExpress.XtraBars.BarButtonItem pdfExportButton;
        private DevExpress.XtraBars.BarButtonItem xlsExportButton;
        private DevExpress.XtraBars.BarButtonItem otherExportButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private DevExpress.XtraBars.BarButtonItem printButton;
        private DevExpress.XtraBars.BarButtonItem deleteItemButton;
        private DevExpress.XtraTab.XtraTabControl pluginTabControl;
        private DevExpress.LookAndFeel.DefaultLookAndFeel lookAndFeel;
        private RemObjects.Hydra.ModuleManager moduleManager;
    }
}
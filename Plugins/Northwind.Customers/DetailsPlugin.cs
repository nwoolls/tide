/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Drawing;
using RemObjects.Hydra;
using System.Windows.Forms;
using System.Collections.Generic;
using tide.Utilities.Plugins;
using tide.PluginInterfaces;

namespace Northwind.Customers
{
    [Plugin, VisualPlugin]
    public partial class DetailsPlugin : DetailWindow
    {
        public DetailsPlugin()
        {
            InitializeComponent();
        }

        #region DetailWindow Members

        public override void DisplayDetails(IMasterWindow sender)
        {
            if (sender is BrowsePlugin)
            {
                RemoveControlErrors();

                customersBindingSource.SuspendBinding();
                customersBindingSource.DataSource = sender.DetailsObject();
                customersBindingSource.ResumeBinding();

                (Host as IHostApplication).DisplayDetailWindow(sender, this);
            }
        }

        public override bool BeforeSaveDetails(IMasterWindow sender)
        {
            if (sender is BrowsePlugin)
            {
                customersBindingSource.EndEdit();
                return dxValidationProvider1.Validate();
            }
            else
                return true;
        }

        public override string Caption()
        {
            return "Customer Details";
        }

        public override Image Glyph()
        {
            return imageCollection1.Images[0];
        }

        #endregion

        private void RemoveControlErrors()
        {
            List<Control> invalidControls = new List<Control>();
            foreach (Control invalidControl in dxValidationProvider1.GetInvalidControls())
                invalidControls.Add(invalidControl);
            foreach (Control invalidControl in invalidControls)
                dxValidationProvider1.RemoveControlError(invalidControl);
        }
    }
}
/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using RemObjects.Hydra;
using tide.PluginInterfaces;
using tide.Utilities.Plugins;
using System.Windows.Forms;
using Northwind.Data;

namespace Northwind.Customers
{
    [Plugin, VisualPlugin]
    public partial class BrowsePlugin : MasterWindow, IRibbonProvider, IPrintableProvider
    {
        private NorthwindDataSet.CustomersRow editingCustomersRow;

        public BrowsePlugin()
        {
            InitializeComponent();
        }

        #region MasterWindow Members

        public override string ItemName()
        {
            return "Customer";
        }

        public override bool NewItemEnabled()
        {
            return true;
        }

        public override void NewItem(object refId)
        {
            editingCustomersRow = editingNorthwindDataSet.Customers.NewCustomersRow();
            InitializeCustomerRow(editingCustomersRow);

            editingNorthwindDataSet.Customers.Rows.Clear();
            editingNorthwindDataSet.Customers.Rows.Add(editingCustomersRow);

            (Host as IHostApplication).DisplayMasterDetails(this);
        }

        public override bool SaveItemEnabled()
        {
            return editingNorthwindDataSet.HasChanges(); 
        }

        public override bool SaveItem()
        {
            customersTableAdapter.Update(editingNorthwindDataSet);
            northwindDataSet.Customers.LoadDataRow(editingCustomersRow.ItemArray, false);
            return true;
        }

        public override void DeleteItem()
        {
            if (MessageBox.Show("Delete the selected customer?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                customersBindingSource.RemoveCurrent();
                customersTableAdapter.Update(northwindDataSet);
            }
        }

        public override bool DeleteItemEnabled()
        {
            return customersBindingSource.Position >= 0;
        }

        public override void EditItem(object id)
        {
            throw new NotImplementedException();
        }

        public override object DetailsObject()
        {
            return editingCustomersRow;
        }

        #endregion

        #region IRibbonProvider Members

        public DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl()
        {
            return ribbonControl1;
        }

        #endregion

        #region IPrintableProvider Members

        public DevExpress.XtraPrinting.IPrintable Printable()
        {
            return gridControl1;
        }

        #endregion

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BrowseNorthwindCustomers();
            ((IHostApplication)Host).DisplayPlugin(this);
        }

        private void BrowseNorthwindCustomers()
        {
            string connectionString = ConnectionString.GetConnectionString(Host as IHostApplication);
            customersTableAdapter.PublicConnection.ConnectionString = connectionString;
            customersTableAdapter.Fill(northwindDataSet.Customers);
            customersView.BestFitColumns();
        }

        private void customersView_DoubleClick(object sender, EventArgs e)
        {
            NorthwindDataSet.CustomersRow customersRow = (NorthwindDataSet.CustomersRow)northwindDataSet.Customers.Rows[customersBindingSource.Position];

            editingNorthwindDataSet.Customers.Rows.Clear();
            editingNorthwindDataSet.Customers.ImportRow(customersRow);
            editingCustomersRow = (NorthwindDataSet.CustomersRow)editingNorthwindDataSet.Customers.Rows[0];

            (Host as IHostApplication).DisplayMasterDetails(this);
        }

        private static void InitializeCustomerRow(NorthwindDataSet.CustomersRow customersRow)
        {
            customersRow.CustomerID = "";
            customersRow.CompanyName = "";
            customersRow.Address = "";
            customersRow.City = "";
            customersRow.ContactName = "";
            customersRow.ContactTitle = "";
            customersRow.Country = "";
            customersRow.Fax = "";
            customersRow.Phone = "";
            customersRow.PostalCode = "";
            customersRow.Region = "";
        }
    }
}
/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Drawing;
using RemObjects.Hydra;
using tide.Utilities.Plugins;
using tide.PluginInterfaces;
using Northwind.Data;

namespace Northwind.Orders
{
    [Plugin, VisualPlugin]
    public partial class CustomerOrdersPlugin : DetailWindow
    {
        public CustomerOrdersPlugin()
        {
            InitializeComponent();
        }

        #region DetailWindow Members

        public override void DisplayDetails(IMasterWindow sender)
        {
            if (sender.DetailsObject() is NorthwindDataSet.CustomersRow)
            {
                NorthwindDataSet.CustomersRow customersRow = (NorthwindDataSet.CustomersRow)(sender.DetailsObject());

                customerOrdersTableAdapter.PublicConnection.ConnectionString = ConnectionString.GetConnectionString(Host as IHostApplication);
                customerOrdersTableAdapter.Fill(northwindDataSet.CustomerOrders, customersRow.CustomerID);

                (Host as IHostApplication).DisplayDetailWindow(sender, this);
            }
        }

        public override string Caption()
        {
            return "Customer Orders";
        }

        public override int Order()
        {
            return 5;
        }

        public override Image Glyph()
        {
            return imageCollection1.Images[0];
        }

        #endregion
        
    }
}
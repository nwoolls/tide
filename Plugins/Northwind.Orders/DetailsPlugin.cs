/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using RemObjects.Hydra;
using tide.Utilities.Plugins;
using tide.PluginInterfaces;
using Northwind.Data;

namespace Northwind.Orders
{
    [Plugin, VisualPlugin]
    public partial class DetailsPlugin : DetailWindow
    {
        public DetailsPlugin()
        {
            InitializeComponent();
        }

        #region DetailWindow Members

        public override void DisplayDetails(IMasterWindow sender)
        {
            if (sender is BrowsePlugin)
            {
                RemoveControlErrors();

                ordersBindingSource.SuspendBinding();
                ordersBindingSource.DataSource = sender.DetailsObject();
                ordersBindingSource.ResumeBinding();

                (Host as IHostApplication).DisplayDetailWindow(sender, this);
            }
        }

        public override bool BeforeSaveDetails(IMasterWindow sender)
        {
            if (sender is BrowsePlugin)
            {
                ordersBindingSource.EndEdit();
                return dxValidationProvider1.Validate();
            }
            else
                return true;
        }

        public override string Caption()
        {
            return "Order Details";
        }

        public override Image Glyph()
        {
            return imageCollection1.Images[0];
        }

        #endregion

        private void RemoveControlErrors()
        {
            List<Control> invalidControls = new List<Control>();
            foreach (Control invalidControl in dxValidationProvider1.GetInvalidControls())
                invalidControls.Add(invalidControl);
            foreach (Control invalidControl in invalidControls)
                dxValidationProvider1.RemoveControlError(invalidControl);
        }

        private void DetailsPlugin_Load(object sender, EventArgs e)
        {
            string connectionString = ConnectionString.GetConnectionString(Host as IHostApplication);

            customersTableAdapter.PublicConnection.ConnectionString = connectionString;
            employeesTableAdapter.PublicConnection.ConnectionString = connectionString;
            shippersTableAdapter.PublicConnection.ConnectionString = connectionString;

            customersTableAdapter.Fill(northwindDataSet.Customers);
            employeesTableAdapter.Fill(northwindDataSet.Employees);
            shippersTableAdapter.Fill(northwindDataSet.Shippers);
        }
    }
}
using System;
using System.Drawing;
using RemObjects.Hydra;
using tide.PluginInterfaces;
using DevExpress.XtraBars.Docking;
using System.Collections.Generic;

namespace tide.Logging
{
    [Plugin, VisualPlugin]
    public partial class LoggingPlugin : VisualPlugin, IDockWindow, ILogger
    {
        private List<LogLine> logLines;

        public LoggingPlugin()
        {
            InitializeComponent();
        }

        #region ILogger Members
        
        public void AddLogLine(string logMessage, DateTime startTime, DateTime endTime, long size)
        {
            if (logLines == null)
                logLines = new List<LogLine>();
            LogLine line = new LogLine { LogMessage = logMessage, StartTime = startTime, EndTime = endTime, Size = size };
            logLines.Add(line);

            if (logLineBindingSource.DataSource != logLines)
                logLineBindingSource.DataSource = logLines;

            logLineBindingSource.ResetBindings(false);

            logLineBindingSource.Position = logLineBindingSource.Count - 1;
        }

        #endregion

        #region IDockWindow Members
        
        public string Caption()
        {
            return "Logging";
        }

        public Image Glyph()
        {
            return imageCollection1.Images[0];
        }

        public DockingStyle DockStyle()
        {
            return DockingStyle.Bottom;
        }

        #endregion

        #region Event Handlers

        private void logView_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column == colDuration)
                if (e.IsGetData)
                    e.Value = logLines[e.ListSourceRowIndex].EndTime - logLines[e.ListSourceRowIndex].StartTime;
        }

        private void logView_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column == colDuration)
            {
                e.DisplayText = NiceDuration((TimeSpan)e.Value);
            }
            if (e.Column == colSize)
            {
                e.DisplayText = NiceSize((long)e.Value);
            }
        }

        private void logView_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            if (e.Column == colDuration)
                e.Info.DisplayText = NiceDuration(new TimeSpan(decimal.ToInt32((decimal)e.Info.Value)));
            else if (e.Column == colSize)
                e.Info.DisplayText = NiceSize(decimal.ToInt32((decimal)e.Info.Value));
        }

        #endregion
        
        private static string NiceSize(long size)
        {
            string[] suffixes = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            int s = 0;

            while (size >= 1024)
            {
                s++;
                size /= 1024;
            }

            return String.Format("{0} {1}", size, suffixes[s]);
        }

        private static string NiceDuration(TimeSpan span)
        {
            string duration = "";
            if (span.Days > 0)
            {
                if (duration != "")
                    duration = String.Format("{0} ", duration);

                if (span.Days > 1)
                    duration = String.Format("{0}{1} days", duration, span.Days);
                else
                    duration = String.Format("{0}{1} day", duration, span.Days);
            }

            if (span.Hours > 0)
            {
                if (duration != "")
                    duration = String.Format("{0} ", duration);

                if (span.Hours > 1)
                    duration = String.Format("{0}{1} hours", duration, span.Hours);
                else
                    duration = String.Format("{0}{1} hour", duration, span.Hours);
            }

            if (span.Minutes > 0)
            {
                if (duration != "")
                    duration = String.Format("{0} ", duration);

                if (span.Minutes > 1)
                    duration = String.Format("{0}{1} minutes", duration, span.Minutes);
                else
                    duration = String.Format("{0}{1} minute", duration, span.Minutes);
            }

            if (span.Seconds > 0)
            {
                if (duration != "")
                    duration = String.Format("{0} ", duration);

                if (span.Seconds > 1)
                    duration = String.Format("{0}{1} seconds", duration, span.Seconds);
                else
                    duration = String.Format("{0}{1} second", duration, span.Seconds);
            }

            if (span.Milliseconds > 0)
            {
                if (duration != "")
                    duration = String.Format("{0} ", duration);

                if (span.Milliseconds > 1)
                    duration = String.Format("{0}{1} milliseconds", duration, span.Milliseconds);
                else
                    duration = String.Format("{0}{1} millisecond", duration, span.Milliseconds);
            }

            return duration;
        }
    }
}
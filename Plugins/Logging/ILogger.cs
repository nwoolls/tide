﻿using System;

namespace tide.Logging
{
    public interface ILogger
    {
        void AddLogLine(string logMessage, DateTime startTime, DateTime endTime, Int64 size);
    }
}

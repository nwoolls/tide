using System;

namespace tide.Logging
{
    public class LogLine : object
    {
        public string LogMessage { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Int64 Size { get; set; }
    }
}

﻿#region Using directives

using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using RemObjects.Hydra;

#endregion

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Logging")]
[assembly: AssemblyDescription("Technician Integrated Data Environment")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Victory Technologies, Inc.")]
[assembly: AssemblyProduct("Victory Technician Services")]
[assembly: AssemblyCopyright("Copyright © Victory Technologies, Inc. 2008-2010")]
[assembly: AssemblyTrademark("Technician Services(tm) Victory Technologies, Inc. 2008-2010")]
[assembly: AssemblyCulture("")]


// The Hydra PluginModule attrubite can specify a name and version for
// the plugin to be shows in host applications
[assembly: PluginModule(Name = "Logging", Version = "1.0.0.1")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

//In order to begin building localizable applications, set 
//<UICulture>CultureYouAreCodingWith</UICulture> in your .csproj file
//inside a <PropertyGroup>.  For example, if you are using US english
//in your source files, set the <UICulture> to en-US.  Then uncomment
//the NeutralResourceLanguage attribute below.  Update the "en-US" in
//the line below to match the UICulture setting in the project file.

//[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.Satellite)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("af48b94b-21fa-4034-be57-8dcd07d28921")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("3.0.4.5")]
[assembly: AssemblyFileVersionAttribute("3.0.4.5")]

/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using RemObjects.Hydra;
using tide.PluginInterfaces;
using tide.Utilities.Settings;
using tide.Utilities.Encryption;
using Microsoft.Data.ConnectionUI;

namespace Northwind.Connection
{
    [Plugin, VisualPlugin]
    public partial class OptionsPlugin : VisualPlugin, IOptionsPage
    {
        private SimpleSettings simpleSettings;
        private SqlConnectionProperties connectionProperties;

        public OptionsPlugin()
        {
            InitializeComponent();
        }

        #region IOptionsPage Members

        public void LoadOptions()
        {
            SetupSettingStorage();

            string connectionString = simpleSettings.GetSetting("Northwind/ConnectionString", "");
            if (connectionString != "")
                connectionString = SimpleEncryption.SimpleDecrypt(connectionString);
            connectionProperties.ConnectionStringBuilder.ConnectionString = connectionString;

            sqlConnectionUIControl1.Initialize(connectionProperties);
            sqlConnectionUIControl1.LoadProperties();
        }

        private void SetupSettingStorage()
        {
            ApplicationSettings applicationSettings = new ApplicationSettings((IHostApplication)Host);
            simpleSettings = new SimpleSettings(applicationSettings.SettingsFileName);
            connectionProperties = new SqlConnectionProperties();
            SimpleEncryption.PassPhrase = "northwindpassphrase";
        }

        public bool SaveOptions()
        {
            string connectionString = connectionProperties.ConnectionStringBuilder.ConnectionString;
            simpleSettings.SetSetting("Northwind/ConnectionString", SimpleEncryption.SimpleEncrypt(connectionString));
            return true;
        }

        public bool SaveOptionsEnabled()
        {
            return true;
        }

        public string Caption()
        {
            return "Northwind Connection";
        }

        #endregion
    }
}
namespace Northwind.Connection
{
    partial class OptionsPlugin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.sqlConnectionUIControl1 = new Microsoft.Data.ConnectionUI.SqlConnectionUIControl();
            this.xtraScrollableControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.sqlConnectionUIControl1);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(457, 408);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // sqlConnectionUIControl1
            // 
            this.sqlConnectionUIControl1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sqlConnectionUIControl1.Location = new System.Drawing.Point(6, 7);
            this.sqlConnectionUIControl1.Margin = new System.Windows.Forms.Padding(0);
            this.sqlConnectionUIControl1.MinimumSize = new System.Drawing.Size(350, 360);
            this.sqlConnectionUIControl1.Name = "sqlConnectionUIControl1";
            this.sqlConnectionUIControl1.Size = new System.Drawing.Size(444, 395);
            this.sqlConnectionUIControl1.TabIndex = 6;
            // 
            // OptionsPlugin
            // 
            this.Actions.Clear();
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "OptionsPlugin";
            this.Size = new System.Drawing.Size(457, 408);
            this.Toolbars.Clear();
            this.Toolbars.Add(new RemObjects.Hydra.Actions.MenuBar("Main Menu", null));
            this.xtraScrollableControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private Microsoft.Data.ConnectionUI.SqlConnectionUIControl sqlConnectionUIControl1;


    }
}
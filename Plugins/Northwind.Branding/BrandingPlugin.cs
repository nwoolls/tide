/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.ComponentModel;
using RemObjects.Hydra;
using tide.PluginInterfaces;

namespace Northwind.Branding
{
    [Plugin, NonVisualPlugin]
    public partial class BrandingPlugin : NonVisualPlugin, IBrandingProvider
    {
        public BrandingPlugin()
        {
            InitializeComponent();
        }

        public BrandingPlugin(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region IBrandingProvider Members

        public string ApplicationName()
        {
            return "Northwind Client";
        }

        public string ShortApplicationName()
        {
            return ApplicationName();
        }

        public string CompanyName()
        {
            return "Widgets and Gadgets, Inc.";
        }

        public string ShortCompanyName()
        {
            return "Widgets and Gadgets";
        }

        public bool RequiresRegistration()
        {
            return false;
        }

        public string ApplicationId()
        {
            return "";
        }

        public string DefaultSkinName()
        {
            return "Sharp Plus";
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Common;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using System.IO;
using DevExpress.Utils;
using tide.Utilities.Forms;

namespace tide.Utilities.Spreadsheets
{
    public partial class ExcelImportControl : XtraUserControl
    {
        private DataSet mappingData;
        private List<string> requiredFields;

        public ExcelImportControl()
        {
            InitializeComponent();
        }

        public void ChooseAFile()
        {
            importTabControl.SelectedTabPage = fileTabPage;
        }

        public void ChooseAStartingRow()
        {
            importTabControl.SelectedTabPage = startRowTabPage;
            PreviewStartRowData();
        }

        public void ChooseASheet()
        {
            importTabControl.SelectedTabPage = sheetTabPage;
            PopulateSheets();
        }

        public bool IsFileChosen()
        {
            return File.Exists(fileEdit.Text);
        }

        public bool IsSheetChosen()
        {
            return sheetCombo.SelectedIndex >= 0;
        }

        private List<string> GetCurrentlyMappedFields()
        {
            List<string> mappedFields = new List<string>();
            foreach (DataColumn column in mappingData.Tables[0].Columns)
            {
                string fieldValue = mappingData.Tables[0].Rows[0][column].ToString();
                if (!string.IsNullOrEmpty(fieldValue))
                    mappedFields.Add(fieldValue);
            }
            return mappedFields;
        }

        public bool AreFieldsMapped()
        {
            if (mappingData != null)
            {
                List<string> mappedFields = GetCurrentlyMappedFields();
                if ((requiredFields == null) || (requiredFields.Count == 0))
                    return mappedFields.Count > 0;
                else
                {
                    List<string> checkingFields = new List<string>(requiredFields);
                    foreach (string mappedField in mappedFields)
                        checkingFields.Remove(mappedField);
                    return checkingFields.Count == 0;
                }
            }
            return false;
        }

        public void MapFields(List<string> fields)
        {
            MapFields(fields, null);
        }

        private void SetRequiredFieldText()
        {
            string requiredText = "";
            if (requiredFields != null)
                for (int i = 0; i < requiredFields.Count; i++)
                {
                    if ((requiredText != "") && (i < requiredFields.Count - 1))
                        requiredText = String.Format("{0}, ", requiredText);
                    if ((i == requiredFields.Count - 1) && (i > 0))
                        requiredText = String.Format("{0} and ", requiredText);
                    requiredText = requiredText + requiredFields[i];
                }
            string captionText = "Map columns";
            if (requiredText != "")
            {
                string verb = requiredFields.Count > 1 ? "are" : "is";
                captionText = String.Format("{0} ({1} {2} required)", captionText, requiredText, verb);
            }
            captionText = String.Format("{0}:", captionText);
            mappingControlItem.Text = captionText;
        }

        public void MapFields(List<string> fields, List<string> requiredFields)
        {
            this.requiredFields = requiredFields;
            importTabControl.SelectedTabPage = mappingTabPage;
            PopulateMappingData(fields);
            SetRequiredFieldText();
        }

        public DataTable GetMappedData()
        {
            DataSet mappedData = GetDataSetForSheet();

            int i = 1;
            int startRow = int.Parse(startSpinEdit.EditValue.ToString());
            while (i < startRow)
            {
                mappedData.Tables[0].Rows[i - 1].Delete();
                i++;
            }
            mappedData.Tables[0].Rows.InsertAt(mappedData.Tables[0].NewRow(), 0);

            for (int j = 0; j < mappingData.Tables[0].Columns.Count; j++)
                mappedData.Tables[0].Rows[0][j] = mappingData.Tables[0].Rows[0][j];
            return mappedData.Tables[0];
        }

        private static string FormatSheetName(string sheetName)
        {
            string newSheetName = sheetName;
            if (newSheetName.EndsWith("'"))
                newSheetName = newSheetName.Substring(0, newSheetName.Length - 1);
            if (newSheetName.StartsWith("'"))
                newSheetName = newSheetName.Substring(1, newSheetName.Length - 1);
            if (newSheetName.EndsWith("$"))
                newSheetName = newSheetName.Substring(0, newSheetName.Length - 1);
            return newSheetName;
        }
        private void PopulateSheets()
        {
            sheetCombo.Properties.Items.Clear();

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");

            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = GetConnectionString();
                connection.Open();
                DataTable tbl = connection.GetSchema("Tables");
                connection.Close();
                
                foreach (DataRow row in tbl.Rows)
                {
                    string sheetName = FormatSheetName((string)row["TABLE_NAME"]);
                    sheetCombo.Properties.Items.Add(sheetName);
                }
            }
        }

        private void fileEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (openSheetDialog.ShowDialog() == DialogResult.OK)
                fileEdit.Text = openSheetDialog.FileName;
        }

        private string GetConnectionString()
        {
            return String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};" +
                "Extended Properties=\"Excel 8.0;HDR=NO\"", fileEdit.Text);
        }

        private DataSet GetDataSetForSheet()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");

            DbConnection connection = null;
            using (connection = factory.CreateConnection())
            {
                connection.ConnectionString = GetConnectionString();

                DbCommand selectCommand = factory.CreateCommand();
                selectCommand.CommandText = string.Format("SELECT * FROM [{0}$]", sheetCombo.Text);
                selectCommand.Connection = connection;

                DbDataAdapter adapter = factory.CreateDataAdapter();
                adapter.SelectCommand = selectCommand;

                DataSet sheetData = new DataSet();

                adapter.Fill(sheetData);

                return sheetData;
            }
        }

        private void PreviewSelectedSheet()
        {
            DataSet sheetData = GetDataSetForSheet();

            sheetGrid.DataSource = sheetData.Tables[0];
            sheetView.Columns.Clear();
            sheetView.PopulateColumns();
        }

        private void sheetCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (new HourGlass())
                PreviewSelectedSheet();
        }

        private void PreviewStartRowData()
        {
            DataSet sheetData = GetDataSetForSheet();

            int i = 1;
            int startRow = int.Parse(startSpinEdit.EditValue.ToString());
            while (i < startRow)
            {
                sheetData.Tables[0].Rows[i - 1].Delete();
                i++;
            }

            startRowGrid.DataSource = sheetData.Tables[0];
            startRowView.Columns.Clear();
            startRowView.PopulateColumns();
        }

        private void SetupMappingColumns(List<string> fields)
        {
            RepositoryItemComboBox item = new RepositoryItemComboBox();
            item.Properties.Items.Add("");
            item.Properties.Items.AddRange(fields.ToArray());
            item.TextEditStyle = TextEditStyles.DisableTextEditor;
            foreach (DevExpress.XtraGrid.Columns.GridColumn column in mappingView.Columns)
                column.ColumnEdit = item;
        }

        private void PopulateMappingData(List<string> fields)
        {
            mappingData = GetDataSetForSheet();

            int i = 1;
            int startRow = int.Parse(startSpinEdit.EditValue.ToString());
            while (i < startRow)
            {
                mappingData.Tables[0].Rows[i - 1].Delete();
                i++;
            }
            mappingData.Tables[0].Rows.InsertAt(mappingData.Tables[0].NewRow(), 0);

            mappingGrid.DataSource = mappingData.Tables[0];
            mappingView.Columns.Clear();
            mappingView.PopulateColumns();

            SetupMappingColumns(fields);
        }

        private void startSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            using (new HourGlass())
                PreviewStartRowData();
        }

        private void ExcelImportControl_Load(object sender, EventArgs e)
        {
            importTabControl.ShowTabHeader = DefaultBoolean.False;
            ChooseAFile();
        }

        private void mappingView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            foreach (DevExpress.XtraGrid.Columns.GridColumn column in mappingView.Columns)
                column.OptionsColumn.AllowEdit = e.FocusedRowHandle == 0;
            mappingView.OptionsSelection.MultiSelect = e.FocusedRowHandle > 0;
        }
    }
}

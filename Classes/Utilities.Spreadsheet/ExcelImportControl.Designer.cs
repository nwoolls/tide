namespace tide.Utilities.Spreadsheets
{
    partial class ExcelImportControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.importTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.sheetTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.sheetGrid = new DevExpress.XtraGrid.GridControl();
            this.sheetView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.sheetCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.fileTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.fileEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.startRowTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.startRowGrid = new DevExpress.XtraGrid.GridControl();
            this.startRowView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.startSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.mappingTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.mappingGrid = new DevExpress.XtraGrid.GridControl();
            this.mappingView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.mappingControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.openSheetDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.importTabControl)).BeginInit();
            this.importTabControl.SuspendLayout();
            this.sheetTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sheetGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sheetView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sheetCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            this.fileTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.startRowTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startRowGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startRowView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.mappingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mappingGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mappingView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mappingControlItem)).BeginInit();
            this.SuspendLayout();
            // 
            // importTabControl
            // 
            this.importTabControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.importTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.importTabControl.Location = new System.Drawing.Point(0, 0);
            this.importTabControl.Name = "importTabControl";
            this.importTabControl.PaintStyleName = "Flat";
            this.importTabControl.SelectedTabPage = this.sheetTabPage;
            this.importTabControl.Size = new System.Drawing.Size(581, 522);
            this.importTabControl.TabIndex = 0;
            this.importTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.fileTabPage,
            this.sheetTabPage,
            this.startRowTabPage,
            this.mappingTabPage});
            // 
            // sheetTabPage
            // 
            this.sheetTabPage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sheetTabPage.Controls.Add(this.layoutControl2);
            this.sheetTabPage.Name = "sheetTabPage";
            this.sheetTabPage.Size = new System.Drawing.Size(581, 500);
            this.sheetTabPage.Text = "Choose Sheet";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl2.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl2.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl2.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl2.Controls.Add(this.sheetGrid);
            this.layoutControl2.Controls.Add(this.sheetCombo);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(581, 500);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // sheetGrid
            // 
            this.sheetGrid.Location = new System.Drawing.Point(7, 56);
            this.sheetGrid.MainView = this.sheetView;
            this.sheetGrid.Name = "sheetGrid";
            this.sheetGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            this.sheetGrid.Size = new System.Drawing.Size(568, 438);
            this.sheetGrid.TabIndex = 5;
            this.sheetGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.sheetView});
            // 
            // sheetView
            // 
            this.sheetView.GridControl = this.sheetGrid;
            this.sheetView.Name = "sheetView";
            this.sheetView.OptionsBehavior.Editable = false;
            this.sheetView.OptionsCustomization.AllowColumnMoving = false;
            this.sheetView.OptionsCustomization.AllowFilter = false;
            this.sheetView.OptionsCustomization.AllowGroup = false;
            this.sheetView.OptionsCustomization.AllowQuickHideColumns = false;
            this.sheetView.OptionsCustomization.AllowSort = false;
            this.sheetView.OptionsSelection.MultiSelect = true;
            this.sheetView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.sheetView.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // sheetCombo
            // 
            this.sheetCombo.Location = new System.Drawing.Point(90, 7);
            this.sheetCombo.Name = "sheetCombo";
            this.sheetCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sheetCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.sheetCombo.Size = new System.Drawing.Size(485, 20);
            this.sheetCombo.StyleController = this.layoutControl2;
            this.sheetCombo.TabIndex = 4;
            this.sheetCombo.SelectedIndexChanged += new System.EventHandler(this.sheetCombo_SelectedIndexChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem22});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(581, 500);
            this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.sheetCombo;
            this.layoutControlItem2.CustomizationFormText = "Sheet to import:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(579, 31);
            this.layoutControlItem2.Text = "Sheet to import:";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.sheetGrid;
            this.layoutControlItem22.CustomizationFormText = "Preview:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(579, 467);
            this.layoutControlItem22.Text = "Preview:";
            this.layoutControlItem22.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(78, 13);
            // 
            // fileTabPage
            // 
            this.fileTabPage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fileTabPage.Controls.Add(this.layoutControl1);
            this.fileTabPage.Name = "fileTabPage";
            this.fileTabPage.Size = new System.Drawing.Size(581, 500);
            this.fileTabPage.Text = "Choose File";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl1.Controls.Add(this.fileEdit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(581, 500);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // fileEdit
            // 
            this.fileEdit.Location = new System.Drawing.Point(78, 7);
            this.fileEdit.Name = "fileEdit";
            this.fileEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.fileEdit.Size = new System.Drawing.Size(497, 20);
            this.fileEdit.StyleController = this.layoutControl1;
            this.fileEdit.TabIndex = 4;
            this.fileEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.fileEdit_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(581, 500);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.fileEdit;
            this.layoutControlItem1.CustomizationFormText = "File to import:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(579, 498);
            this.layoutControlItem1.Text = "File to import:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(66, 13);
            // 
            // startRowTabPage
            // 
            this.startRowTabPage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.startRowTabPage.Controls.Add(this.layoutControl3);
            this.startRowTabPage.Name = "startRowTabPage";
            this.startRowTabPage.Size = new System.Drawing.Size(581, 500);
            this.startRowTabPage.Text = "Choose Start Row";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl3.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl3.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl3.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl3.Controls.Add(this.startRowGrid);
            this.layoutControl3.Controls.Add(this.startSpinEdit);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(581, 500);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // startRowGrid
            // 
            this.startRowGrid.Location = new System.Drawing.Point(7, 56);
            this.startRowGrid.MainView = this.startRowView;
            this.startRowGrid.Name = "startRowGrid";
            this.startRowGrid.Size = new System.Drawing.Size(568, 438);
            this.startRowGrid.TabIndex = 5;
            this.startRowGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.startRowView});
            // 
            // startRowView
            // 
            this.startRowView.GridControl = this.startRowGrid;
            this.startRowView.Name = "startRowView";
            this.startRowView.OptionsBehavior.Editable = false;
            this.startRowView.OptionsCustomization.AllowColumnMoving = false;
            this.startRowView.OptionsCustomization.AllowFilter = false;
            this.startRowView.OptionsCustomization.AllowGroup = false;
            this.startRowView.OptionsCustomization.AllowQuickHideColumns = false;
            this.startRowView.OptionsCustomization.AllowSort = false;
            this.startRowView.OptionsSelection.MultiSelect = true;
            this.startRowView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.startRowView.OptionsView.ColumnAutoWidth = false;
            this.startRowView.OptionsView.ShowGroupPanel = false;
            // 
            // startSpinEdit
            // 
            this.startSpinEdit.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.startSpinEdit.Location = new System.Drawing.Point(104, 7);
            this.startSpinEdit.Name = "startSpinEdit";
            this.startSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.startSpinEdit.Properties.IsFloatValue = false;
            this.startSpinEdit.Properties.Mask.EditMask = "N00";
            this.startSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.startSpinEdit.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.startSpinEdit.Size = new System.Drawing.Size(471, 20);
            this.startSpinEdit.StyleController = this.layoutControl3;
            this.startSpinEdit.TabIndex = 4;
            this.startSpinEdit.EditValueChanged += new System.EventHandler(this.startSpinEdit_EditValueChanged);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(581, 500);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.startSpinEdit;
            this.layoutControlItem4.CustomizationFormText = "Data starts at row:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(579, 31);
            this.layoutControlItem4.Text = "Data starts at row:";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.startRowGrid;
            this.layoutControlItem5.CustomizationFormText = "Preview:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(579, 467);
            this.layoutControlItem5.Text = "Preview:";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(92, 13);
            // 
            // mappingTabPage
            // 
            this.mappingTabPage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mappingTabPage.Controls.Add(this.layoutControl4);
            this.mappingTabPage.Name = "mappingTabPage";
            this.mappingTabPage.Size = new System.Drawing.Size(581, 500);
            this.mappingTabPage.Text = "Map Columns";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl4.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl4.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl4.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl4.Controls.Add(this.mappingGrid);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(581, 500);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // mappingGrid
            // 
            this.mappingGrid.Location = new System.Drawing.Point(7, 25);
            this.mappingGrid.MainView = this.mappingView;
            this.mappingGrid.Name = "mappingGrid";
            this.mappingGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox2});
            this.mappingGrid.Size = new System.Drawing.Size(568, 469);
            this.mappingGrid.TabIndex = 4;
            this.mappingGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mappingView});
            // 
            // mappingView
            // 
            this.mappingView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.mappingView.GridControl = this.mappingGrid;
            this.mappingView.Name = "mappingView";
            this.mappingView.OptionsCustomization.AllowColumnMoving = false;
            this.mappingView.OptionsCustomization.AllowFilter = false;
            this.mappingView.OptionsCustomization.AllowGroup = false;
            this.mappingView.OptionsCustomization.AllowQuickHideColumns = false;
            this.mappingView.OptionsCustomization.AllowSort = false;
            this.mappingView.OptionsSelection.MultiSelect = true;
            this.mappingView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.mappingView.OptionsView.ColumnAutoWidth = false;
            this.mappingView.OptionsView.ShowGroupPanel = false;
            this.mappingView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.mappingView_FocusedRowChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.ColumnEdit = this.repositoryItemComboBox2;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.mappingControlItem});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(581, 500);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // mappingControlItem
            // 
            this.mappingControlItem.Control = this.mappingGrid;
            this.mappingControlItem.CustomizationFormText = "Map columns:";
            this.mappingControlItem.Location = new System.Drawing.Point(0, 0);
            this.mappingControlItem.Name = "mappingControlItem";
            this.mappingControlItem.Size = new System.Drawing.Size(579, 498);
            this.mappingControlItem.Text = "Map columns:";
            this.mappingControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.mappingControlItem.TextSize = new System.Drawing.Size(65, 13);
            // 
            // openSheetDialog
            // 
            this.openSheetDialog.DefaultExt = "xls";
            this.openSheetDialog.Filter = "Spreadsheets|*.xls";
            // 
            // ExcelImportControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.importTabControl);
            this.Name = "ExcelImportControl";
            this.Size = new System.Drawing.Size(581, 522);
            this.Load += new System.EventHandler(this.ExcelImportControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.importTabControl)).EndInit();
            this.importTabControl.ResumeLayout(false);
            this.sheetTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sheetGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sheetView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sheetCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            this.fileTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.startRowTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.startRowGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startRowView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.mappingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mappingGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mappingView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mappingControlItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl importTabControl;
        private DevExpress.XtraTab.XtraTabPage sheetTabPage;
        private DevExpress.XtraTab.XtraTabPage startRowTabPage;
        private DevExpress.XtraTab.XtraTabPage mappingTabPage;
        private DevExpress.XtraTab.XtraTabPage fileTabPage;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.ComboBoxEdit sheetCombo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ButtonEdit fileEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraGrid.GridControl startRowGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView startRowView;
        private DevExpress.XtraEditors.SpinEdit startSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraGrid.GridControl mappingGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView mappingView;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem mappingControlItem;
        private System.Windows.Forms.OpenFileDialog openSheetDialog;
        private DevExpress.XtraGrid.GridControl sheetGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView sheetView;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
    }
}

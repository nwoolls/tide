﻿/* HourGlass class sample provided by Microsoft MVP nobugz
 * Sample source posted without license
 * Sources: 
 * http://stackoverflow.com/questions/302663/cursor-current-vs-this-cursor-in-net-c
 * http://social.msdn.microsoft.com/forums/en-US/winforms/thread/18b8b65e-03cd-4a4f-9b7e-4e44ea5ac5df/
 * http://social.msdn.microsoft.com/Forums/en-US/winforms/thread/6d16bc76-e286-49e7-b4c2-5bc35e9f0d54
 * 
 * No contact information could be found for nobugz - contact nwoolls@gmail.com if redistribution is not permissable
 * Copyright "nobugz" - all rights reserved */

using System;
using System.Windows.Forms;

namespace tide.Utilities.Forms
{
    public class HourGlass : IDisposable
    {
        public HourGlass()
        {
            Enabled = true;
        }

        public void Dispose()
        {
            Enabled = false;
        }

        public static bool Enabled
        {
            get
            {
                return Application.UseWaitCursor;
            }
            set
            {
                if (value == Application.UseWaitCursor) return;
                Application.UseWaitCursor = value;
                Form f = Form.ActiveForm;
                if (f != null && f.Handle != null)   // Send WM_SETCURSOR
                    SendMessage(f.Handle, 0x20, f.Handle, (IntPtr)1);
            }
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
    }
}

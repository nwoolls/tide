/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.ComponentModel;
using System.Windows.Forms;
using RemObjects.Hydra;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using tide.PluginInterfaces;
using tide.Utilities.Settings;
using System.Reflection;

namespace tide.Utilities.Plugins
{
    [Plugin, VisualPlugin]
    public partial class BaseWindow : VisualPlugin, IInitializable
    {
        public BaseWindow()
        {
            InitializeComponent();
        }

        #region IInitializable Members

        public virtual void Initialize()
        {
            LoadControlLayouts();
            LoadGridLayouts();
            SetupDataBindings();
            SetupGrids();
            SetupComboEdits();
        }

        public virtual void Uninitialize()
        {
            SaveGridLayouts();
            SaveControlLayouts();
        }

        #endregion

        private ApplicationSettings applicationSettings;
        private ApplicationSettings ApplicationSettings
        {
            get
            {
                if (applicationSettings == null)
                    applicationSettings = new ApplicationSettings(Host as IHostApplication);
                return applicationSettings;
            }
        }

        protected string GetComponentFileName(Component component, string parentFolder, string fileName, string layoutVersion)
        {
            string prefix = "";
            if (component is Control)
            {
                Control control = ((Control)component).Parent;
                while ((control != null) && (control.Name != "HostPanel"))
                {
                    if (prefix != "")
                        prefix = String.Format(".{0}", prefix);
                    prefix = control.Name + prefix;
                    control = control.Parent;
                }
            }
            string assemblyName = Assembly.GetAssembly(GetType()).GetName().Name;
            prefix = String.Format("{0}.{1}", assemblyName, prefix);

            string filePath = String.Format("{0}{1}{2}.{3}.{4}.xml", ApplicationSettings.SettingsPath,
                parentFolder, prefix, fileName, layoutVersion);
            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filePath));
            return filePath;
        }

        private void SaveControlLayoutIf(Component component)
        {
            if (component is LayoutControl)
            {
                string filePath = GetComponentFileName(component,"Layouts\\Controls\\", ((LayoutControl)component).Name, 
                    ((LayoutControl)component).LayoutVersion);
                ((LayoutControl)component).SaveLayoutToXml(filePath);
            }
        }

        private void RestoreControlLayoutIf(Component component)
        {
            if (component is LayoutControl)
            {
                string filePath = GetComponentFileName(component, "Layouts\\Controls\\", ((LayoutControl)component).Name,
                    ((LayoutControl)component).LayoutVersion);
                if (System.IO.File.Exists(filePath))
                    ((LayoutControl)component).RestoreLayoutFromXml(filePath);
            }
        }

        private void SaveGridLayoutIf(Component component)
        {
            if (component is GridControl)
            {
                GridControl gridControl = (GridControl)component;
                foreach (BaseView view in gridControl.Views)
                {
                    string filePath = GetComponentFileName(component, "Layouts\\Grids\\", String.Format("{0}.{1}", gridControl.Name, view.Name),
                        view.OptionsLayout.LayoutVersion);
                    view.SaveLayoutToXml(filePath);
                }
            }
        }

        private void RestoreGridLayoutIf(Component component)
        {
            if (component is GridControl)
            {
                GridControl gridControl = (GridControl)component;
                foreach (BaseView view in gridControl.Views)
                {
                    string filePath = GetComponentFileName(component, "Layouts\\Grids\\", String.Format("{0}.{1}", gridControl.Name, view.Name),
                        view.OptionsLayout.LayoutVersion);
                    if (System.IO.File.Exists(filePath))
                        view.RestoreLayoutFromXml(filePath);
                }
            }
        }        

        private static void SetupDataBindingsIf(Component component)
        {
            if (component is Control)
            {
                Control control = (Control)component;
                control.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                foreach (Binding dataBinding in control.DataBindings)
                    dataBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            }
        }

        private static void HandleDataSourceChanged(object sender, EventArgs e)
        {
            GridView gridView = (GridView)sender;
            foreach (GridColumn column in gridView.Columns)
                if (column.ColumnType == typeof(DateTime))
                    column.OptionsFilter.FilterPopupMode = FilterPopupMode.DateSmart;
                else
                    column.OptionsFilter.FilterPopupMode = FilterPopupMode.CheckedList;
        }

        private static void FooterItemClicked(object sender, EventArgs e)
        {
            DXMenuCheckItem menuItem = (DXMenuCheckItem)sender;
            GridView gridView = (GridView)menuItem.Tag;
            gridView.OptionsView.ShowFooter = menuItem.Checked;
        }

        private static void GroupFooterItemClicked(object sender, EventArgs e)
        {
            DXMenuCheckItem menuItem = (DXMenuCheckItem)sender;
            GridView gridView = (GridView)menuItem.Tag;
            if (menuItem.Checked)
                gridView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways;
            else
                gridView.GroupFooterShowMode = GroupFooterShowMode.Hidden;
        }

        private static void HandleShowGridMenu(object sender, GridMenuEventArgs e)
        {
            if (e.MenuType == GridMenuType.Column && e.HitInfo.Column.View is GridView)
            {
                GridView view = (GridView)e.HitInfo.Column.View;

                DXMenuCheckItem item = new DXMenuCheckItem();
                item.Caption = "Footer";
                item.Checked = view.OptionsView.ShowFooter;
                item.Click += FooterItemClicked;
                item.BeginGroup = true;
                item.Tag = view;
                e.Menu.Items.Add(item);

                item = new DXMenuCheckItem();
                item.Caption = "Group Footers";
                item.Checked = view.GroupFooterShowMode == GroupFooterShowMode.VisibleAlways;
                item.Click += GroupFooterItemClicked;
                item.Tag = view;
                e.Menu.Items.Add(item);
            }
        }

        private static void SetupGridIf(Component component)
        {
            if (component is GridControl)
            {
                GridControl gridControl = (GridControl)component;
                foreach (BaseView view in gridControl.Views)
                    if (view is GridView)
                    {
                        GridView gridView = (GridView)view;

                        gridView.OptionsView.ShowGroupPanel = false;
                        gridView.OptionsBehavior.AutoExpandAllGroups = true;

                        //to get Footer and Group Footer in the popup menu
                        gridView.ShowGridMenu += HandleShowGridMenu;

                        //so we can get column data types and assign filter popup mode
                        gridView.DataSourceChanged += HandleDataSourceChanged;

                        //add quick column customization like the VCL grid
                        new DevExpress.XtraGrid.Helpers.GridViewQuickColumnCustomization(gridView);

                        //we're doing a single-click-to-edit paradigm - no incremental search
                        gridView.OptionsBehavior.AllowIncrementalSearch = false;

                        //otherwise the column filter dropdown is limited to 25
                        gridView.OptionsFilter.MaxCheckedListItemCount = Int32.MaxValue;
                    }
            }
        }

        private static void SetupComboEditIf(Component component)
        {
            if (component is ComboBoxEdit)
            {
                ComboBoxEdit comboBoxEdit = (ComboBoxEdit)component;
                comboBoxEdit.Properties.PopupSizeable = true;
            }
        }

        private delegate void ProcessComponent(Component component);

        private static void WalkControls(Component parent, ProcessComponent processComponent)
        {
            processComponent(parent);
            if (parent is Control)
            {
                Control parentControl = (Control)parent;
                foreach (Control child in parentControl.Controls)
                    WalkControls(child, processComponent);
            }
        }

        private void LoadControlLayouts()
        {
            WalkControls(this, RestoreControlLayoutIf);
        }

        private void LoadGridLayouts()
        {
            WalkControls(this, RestoreGridLayoutIf);
        }

        private void SetupDataBindings()
        {
            WalkControls(this, SetupDataBindingsIf);
        }

        private void SetupGrids()
        {
            WalkControls(this, SetupGridIf);
        }

        private void SetupComboEdits()
        {
            WalkControls(this, SetupComboEditIf);
        }

        private void SaveControlLayouts()
        {
            WalkControls(this, SaveControlLayoutIf);
        }

        private void SaveGridLayouts()
        {
            WalkControls(this, SaveGridLayoutIf);
        }
    }
}
﻿/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using tide.PluginInterfaces;
using tide.Utilities.Plugins;

namespace tide.Utilities.Settings
{
    public class ApplicationSettings
    {
        private IHostApplication hostApplication;
        public ApplicationSettings(IHostApplication hostApplication)
        {
            this.hostApplication = hostApplication;
        }

        public string SettingsFileName
        {
            get
            {
                return String.Format("{0}\\settings.xml", SettingsPath);
            }
        }

        public string SettingsPath
        {
            get
            {
                return String.Format("{0}\\{1}\\tide\\{2}\\3.0\\",
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    BrandingProvider.CompanyName(), BrandingProvider.ApplicationName());
            }
        }

        IBrandingProvider brandingProvider;
        private IBrandingProvider BrandingProvider
        {
            get
            {
                if (brandingProvider == null)
                    brandingProvider = PluginRouter.GetPlugin<IBrandingProvider>();
                return brandingProvider;
            }
        }

        PluginRouter pluginRouter;
        private PluginRouter PluginRouter
        {
            get
            {
                if (pluginRouter == null)
                    pluginRouter = new PluginRouter(hostApplication);
                return pluginRouter;
            }
        }
    }
}

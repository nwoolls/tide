using System;
using RemObjects.Hydra;
using tide.Utilities.Plugins;
using tide.PluginInterfaces;

namespace tide.Utilities.Plugins
{
    [Plugin, VisualPlugin]
    public partial class MasterWindow : BaseWindow, IMasterWindow
    {
        public MasterWindow()
        {
            InitializeComponent();
        }

        #region IMasterWindow Members

        public virtual string ItemName()
        {
            return "";
        }

        public virtual bool NewItemEnabled()
        {
            return false;
        }

        public virtual void NewItem(object refId)
        {
            return;
        }

        public virtual bool SaveItemEnabled()
        {
            return false;
        }

        public virtual bool SaveItem()
        {
            return false;
        }

        public virtual void DeleteItem()
        {
            return;
        }

        public virtual bool DeleteItemEnabled()
        {
            return false;
        }

        public virtual void EditItem(object id)
        {
            return;
        }

        public virtual object DetailsObject()
        {
            return null;
        }

        #endregion
    }
}
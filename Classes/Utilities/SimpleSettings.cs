﻿/* QuicNDirtyXML class sample provided by circumpunct
 * Sample source posted without license
 * Sources: 
 * http://www.codeproject.com/KB/XML/quickndirtyxml.aspx
 * 
 * No contact information could be found for circumpunct - contact nwoolls@gmail.com if redistribution is not permitted
 * Copyright "circumpunct" - all rights reserved */

using System;
using System.Xml;
using System.IO;

namespace tide.Utilities.Settings
{
    public class SimpleSettings
    {
        private XmlDocument xmlDocument = new XmlDocument();
        private string documentPath;

        public SimpleSettings(string settingsPath)
        {
            documentPath = settingsPath;
            try 
            { 
                xmlDocument.Load(documentPath); 
            }
            catch 
            { 
                xmlDocument.LoadXml("<settings></settings>"); 
            }
        }

        public int GetSetting(string xPath, int defaultValue)
        { 
            return Convert.ToInt16(GetSetting(xPath, Convert.ToString(defaultValue))); 
        }

        public void SetSetting(string xPath, int value)
        { 
            SetSetting(xPath, Convert.ToString(value));
        }

        public bool GetSetting(string xPath, bool defaultValue)
        {
            return Convert.ToBoolean(GetSetting(xPath, Convert.ToString(defaultValue)));
        }

        public void SetSetting(string xPath, bool value)
        {
            SetSetting(xPath, Convert.ToString(value));
        }

        public string GetSetting(string xPath, string defaultValue)
        {
            XmlNode xmlNode = xmlDocument.SelectSingleNode(String.Format("settings/{0}", xPath));
            if (xmlNode != null)
                return xmlNode.InnerText;
            else
                return defaultValue;
        }

        public void SetSetting(string xPath, string value)
        {
            XmlNode xmlNode = xmlDocument.SelectSingleNode(String.Format("settings/{0}", xPath));
            if (xmlNode == null)
                xmlNode = createMissingNode(String.Format("settings/{0}", xPath));
            xmlNode.InnerText = value;
            Directory.CreateDirectory(Path.GetDirectoryName(documentPath));
            xmlDocument.Save(documentPath);
        }

        private XmlNode createMissingNode(string xPath)
        {
            string[] xPathSections = xPath.Split('/');
            string currentXPath = "";
            XmlNode testNode = null;
            XmlNode currentNode = xmlDocument.SelectSingleNode("settings");
            foreach (string xPathSection in xPathSections)
            {
                currentXPath += xPathSection;
                testNode = xmlDocument.SelectSingleNode(currentXPath);
                if (testNode == null)
                    currentNode.InnerXml += String.Format("<{0}></{0}>", xPathSection);
                currentNode = xmlDocument.SelectSingleNode(currentXPath);
                currentXPath += "/";
            }
            return currentNode;
        }
    }
}
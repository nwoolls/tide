﻿/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Collections.Generic;
using RemObjects.Hydra.CrossPlatform;
using DevExpress.XtraNavBar;
using tide.PluginInterfaces;

namespace tide.Controllers
{
    public class NavBarController
    {
        public NavBarController(NavBarControl navBarControl, List<IHYCrossPlatformPlugin> plugins)
        {
            BuildNavBar(navBarControl, plugins);
        }

        private static void BuildNavBar(NavBarControl hostNavBarControl, List<IHYCrossPlatformPlugin> plugins)
        {
            foreach (IHYCrossPlatformInterface plugin in plugins)
                if (plugin is INavBarProvider)
                    MergeNavBar(hostNavBarControl, ((INavBarProvider)plugin).NavBarControl());
            hostNavBarControl.Visible = hostNavBarControl.Groups.Count > 0;
        }

        private static void MergeNavBar(NavBarControl hostNavBarControl, NavBarControl pluginNavBarControl)
        {
            hostNavBarControl.BeginUpdate();
            try
            {
                foreach (NavBarGroup pluginGroup in pluginNavBarControl.Groups)
                {
                    NavBarGroup newGroup = GetExistingNavBarGroup(hostNavBarControl, pluginGroup);

                    if (newGroup == null)
                        newGroup = CreateNewNavBarGroup(hostNavBarControl, pluginGroup);

                    foreach (NavBarItemLink itm in pluginGroup.ItemLinks)
                        newGroup.ItemLinks.Add(itm.Item);
                }
            }
            finally
            {
                hostNavBarControl.EndUpdate();
            }
            pluginNavBarControl.Visible = false;
        }

        private static NavBarGroup CreateNewNavBarGroup(NavBarControl hostNavBarControl, NavBarGroup fromGroup)
        {
            NavBarGroup newGroup = new NavBarGroup(fromGroup.Caption)
            {
                Tag = fromGroup.Tag,
                SmallImage = fromGroup.SmallImage,
                LargeImage = fromGroup.LargeImage
            };

            hostNavBarControl.Groups.Add(newGroup);
            return newGroup;
        }

        private static NavBarGroup GetExistingNavBarGroup(NavBarControl hostNavBarControl, NavBarGroup fromGroup)
        {
            foreach (NavBarGroup existingGroup in hostNavBarControl.Groups)
                if (existingGroup.Caption.Equals(fromGroup.Caption, StringComparison.CurrentCultureIgnoreCase))
                    return existingGroup;

            return null;
        }
    }
}

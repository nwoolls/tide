namespace tide
{
    partial class PluginPageControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.holderPanel = new DevExpress.XtraEditors.PanelControl();
            this.splitterControl = new DevExpress.XtraEditors.SplitterControl();
            this.detailsTabControl = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this.holderPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsTabControl)).BeginInit();
            this.SuspendLayout();
            // 
            // holderPanel
            // 
            this.holderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.holderPanel.Location = new System.Drawing.Point(0, 0);
            this.holderPanel.Name = "holderPanel";
            this.holderPanel.Size = new System.Drawing.Size(529, 614);
            this.holderPanel.TabIndex = 11;
            // 
            // splitterControl
            // 
            this.splitterControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl.Location = new System.Drawing.Point(529, 0);
            this.splitterControl.Name = "splitterControl";
            this.splitterControl.Size = new System.Drawing.Size(6, 614);
            this.splitterControl.TabIndex = 10;
            this.splitterControl.TabStop = false;
            this.splitterControl.Visible = false;
            // 
            // detailsTabControl
            // 
            this.detailsTabControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.detailsTabControl.Location = new System.Drawing.Point(535, 0);
            this.detailsTabControl.Name = "detailsTabControl";
            this.detailsTabControl.Size = new System.Drawing.Size(359, 614);
            this.detailsTabControl.TabIndex = 9;
            this.detailsTabControl.Visible = false;
            // 
            // PluginPageControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.holderPanel);
            this.Controls.Add(this.splitterControl);
            this.Controls.Add(this.detailsTabControl);
            this.Name = "PluginPageControl";
            this.Size = new System.Drawing.Size(894, 614);
            ((System.ComponentModel.ISupportInitialize)(this.holderPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailsTabControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.PanelControl holderPanel;
        public DevExpress.XtraEditors.SplitterControl splitterControl;
        public DevExpress.XtraTab.XtraTabControl detailsTabControl;




    }
}

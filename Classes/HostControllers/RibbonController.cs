﻿/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Collections.Generic;
using DevExpress.XtraBars.Ribbon;
using RemObjects.Hydra.CrossPlatform;
using tide.PluginInterfaces;

namespace tide.Controllers
{
    public class RibbonController
    {
        public RibbonController(RibbonControl ribbonControl, List<IHYCrossPlatformPlugin> plugins)
        {
            BuildRibbon(ribbonControl, plugins);
        }

        private static void BuildRibbon(RibbonControl ribbonControl, List<IHYCrossPlatformPlugin> plugins)
        {
            List<RibbonControl> ribbons = new List<RibbonControl>();
            foreach (IHYCrossPlatformInterface plugin in plugins)
                if (plugin is IRibbonProvider)
                    ribbons.Add(((IRibbonProvider)plugin).RibbonControl());

            MergeRibbons(ribbonControl, ribbons);
        }

        private static void MergeRibbons(RibbonControl ribbonControl, List<RibbonControl> ribbons)
        {
            for (int i = ribbons.Count - 1; i > 0; i--)
                ribbons[i - 1].MergeRibbon(ribbons[i]);

            if (ribbons.Count > 0)
                ribbonControl.MergeRibbon(ribbons[0]);
        }
    }
}

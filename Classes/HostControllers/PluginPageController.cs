﻿/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Collections.Generic;
using DevExpress.XtraTab;
using RemObjects.Hydra.CrossPlatform;
using tide.PluginInterfaces;
using RemObjects.Hydra;
using System.Windows.Forms;
using DevExpress.XtraSpellChecker;

namespace tide.Controllers
{
    public class PluginPageController
    {
        private XtraTabControl pluginTabControl;
        private List<IHYCrossPlatformPlugin> plugins;

        public PluginPageController(XtraTabControl pluginTabControl, List<IHYCrossPlatformPlugin> plugins)
        {
            this.pluginTabControl = pluginTabControl;
            this.plugins = plugins;
        }

        public IVisualPlugin ActivePlugin()
        {
            if (pluginTabControl.SelectedTabPageIndex >= 0)
            {
                XtraTabPage page = pluginTabControl.SelectedTabPage;
                PluginPageControl pluginControl = (PluginPageControl)page.Controls[0];
                HostPanel hostPanel = (HostPanel)pluginControl.holderPanel.Controls[0];
                IVisualPlugin pagePlugin = (IVisualPlugin)hostPanel.Tag;
                return pagePlugin;
            }
            return null;
        }

        public void DisplayPlugin(IVisualPlugin plugin)
        {
            ActivatePlugin(plugin);

            XtraTabPage existingPage = FindPageForPlugin(plugin);
            if (existingPage != null)
            {
                pluginTabControl.SelectedTabPage = existingPage;
                return;
            }

            XtraTabPage newPage = new XtraTabPage();

            PluginPageControl newPluginControl = new PluginPageControl { Dock = DockStyle.Fill, Parent = newPage };

            HostPanel newHostPanel = new HostPanel { Dock = DockStyle.Fill, Parent = newPluginControl.holderPanel, Tag = plugin };

            newHostPanel.HostPlugin(plugin);

            CreateSpellChecker(newHostPanel);

            pluginTabControl.TabPages.Add(newPage);
            pluginTabControl.SelectedTabPage = newPage;
        }

        private List<object> activated = new List<object>();
        private void ActivatePlugin(object plugin)
        {
            if (plugin is IActivatable)
            {
                if (activated.IndexOf(plugin) == -1)
                {
                    ((IActivatable)plugin).FirstActivate();
                    activated.Add(plugin);
                }
                ((IActivatable)plugin).Activate();
            }
        }

        private XtraTabPage FindPageForPlugin(object plugin)
        {
            foreach (XtraTabPage page in pluginTabControl.TabPages)
            {
                PluginPageControl pluginControl = (PluginPageControl)page.Controls[0];
                HostPanel hostPanel = (HostPanel)pluginControl.holderPanel.Controls[0];
                IVisualPlugin pagePlugin = (IVisualPlugin)hostPanel.Tag;
                if (pagePlugin == plugin)
                    return page;
            }
            return null;
        }

        private List<SpellChecker> spellCheckers = new List<SpellChecker>();
        private void CreateSpellChecker(Control parentContainer)
        {
            SpellChecker spellChecker = new SpellChecker();
            spellCheckers.Add(spellChecker);
            spellChecker.SpellCheckMode = SpellCheckMode.AsYouType;
            spellChecker.ParentContainer = parentContainer;
            spellChecker.CheckAsYouTypeOptions.CheckControlsInParentContainer = true;
        }

        public void DisplayMasterDetails(IMasterWindow parent)
        {
            DisplayDetails(parent, SortedDetailPlugins);
        }

        private static void DisplayDetails(IMasterWindow parent, List<IHYCrossPlatformInterface> plugins)
        {
            foreach (IHYCrossPlatformInterface plugin in plugins)
                if (plugin is IDetailWindow)
                    ((IDetailWindow)plugin).DisplayDetails(parent);
        }

        private List<IHYCrossPlatformInterface> sortedDetailPlugins;
        private List<IHYCrossPlatformInterface> SortedDetailPlugins
        {
            get
            {
                if (sortedDetailPlugins == null)
                {
                    sortedDetailPlugins = new List<IHYCrossPlatformInterface>();
                    foreach (IHYCrossPlatformInterface plugin in plugins)
                        if (plugin is IDetailWindow)
                            sortedDetailPlugins.Add(plugin);

                    sortedDetailPlugins.Sort(ComparePluginOrder);
                }
                return sortedDetailPlugins;
            }
        }

        private static int ComparePluginOrder(IHYCrossPlatformInterface intf1, IHYCrossPlatformInterface intf2)
        {
            return ((IDetailWindow)intf1).Order().CompareTo(((IDetailWindow)intf2).Order());
        }

        public void DisplayDetailWindow(IMasterWindow parent, IDetailWindow plugin)
        {
            if (parent is IVisualPlugin)
              DisplayPlugin((IVisualPlugin)parent);

            XtraTabPage parentPage = FindPageForPlugin(parent);

            if (parentPage != null)
            {
                PluginPageControl pluginControl = (PluginPageControl)parentPage.Controls[0];
                DisplayDetailsInTabControl(pluginControl.detailsTabControl, plugin);
                pluginControl.splitterControl.Visible = true;
                pluginControl.detailsTabControl.Visible = true;
            }
        }

        private void DisplayDetailsInTabControl(XtraTabControl detailsTabControl, IDetailWindow plugin)
        {
            ActivatePlugin(plugin);

            if (TabExistsForDetailsWindow(detailsTabControl, plugin))
                return;

            int index = GetIndexForNewDetailsTab(detailsTabControl, plugin);

            XtraTabPage newPage = new XtraTabPage { Text = plugin.Caption(), Image = plugin.Glyph() };

            detailsTabControl.TabPages.Insert(index, newPage);

            HostPanel newHostPanel = new HostPanel { Dock = DockStyle.Fill, Parent = newPage, Tag = plugin };

            CreateSpellChecker(newHostPanel);

            if (plugin is IVisualPlugin)
                newHostPanel.HostPlugin((IVisualPlugin)plugin);
        }

        private static bool TabExistsForDetailsWindow(XtraTabControl detailsTabControl, IDetailWindow plugin)
        {
            for (int i = 0; i < detailsTabControl.TabPages.Count; i++)
            {
                HostPanel hostPanel = (HostPanel)((XtraTabPage)detailsTabControl.TabPages[i]).Controls[0];
                IVisualPlugin existingPlugin = (IVisualPlugin)hostPanel.Tag;
                if (existingPlugin == plugin)
                    return true;
            }
            return false;
        }

        private static int GetIndexForNewDetailsTab(XtraTabControl detailsTabControl, IDetailWindow plugin)
        {
            int index = detailsTabControl.TabPages.Count;
            for (int i = 0; i < detailsTabControl.TabPages.Count; i++)
            {
                HostPanel hostPanel = (HostPanel)((XtraTabPage)detailsTabControl.TabPages[i]).Controls[0];
                IVisualPlugin existingPlugin = (IVisualPlugin)hostPanel.Tag;
                if ((existingPlugin is IDetailWindow) && (((IDetailWindow)existingPlugin).Order() >= plugin.Order()) && (index > i))
                    index = i;
            }
            return index;
        }
    }
}

﻿/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Collections.Generic;
using DevExpress.XtraBars.Docking;
using RemObjects.Hydra.CrossPlatform;
using tide.PluginInterfaces;
using RemObjects.Hydra;
using System.Windows.Forms;
using DevExpress.XtraSpellChecker;
using System.ComponentModel;

namespace tide.Controllers
{
    public partial class DockManagerController : Component
    {
        public DockManagerController(DockManager dockManager, List<IHYCrossPlatformPlugin> plugins)
        {
            InitializeComponent();
            BuildDockPanels(dockManager, plugins);
        }

        private void BuildDockPanels(DockManager dockManager, List<IHYCrossPlatformPlugin> plugins)
        {
            foreach (IHYCrossPlatformInterface plugin in plugins)
                if (plugin is IDockWindow)
                {
                    DockPanel newPanel = CreateNewDockPanel(dockManager, ((IDockWindow)plugin));

                    DockPanelToExistingPanel(dockManager, ((IDockWindow)plugin).DockStyle(), newPanel);

                    HostPanel newHostPanel = new HostPanel { Dock = DockStyle.Fill, Parent = newPanel, Tag = plugin };

                    CreateSpellChecker(newHostPanel);

                    if (plugin is IVisualPlugin)
                        newHostPanel.HostPlugin((IVisualPlugin)plugin);
                }
        }

        private DockPanel CreateNewDockPanel(DockManager dockManager, IDockWindow dockWindow)
        {
            DockPanel newPanel = dockManager.AddPanel(dockWindow.DockStyle());

            newPanel.Text = (dockWindow.Caption());
            dockImages.AddImage(dockWindow.Glyph());
            newPanel.ImageIndex = dockImages.Images.Count - 1;
            return newPanel;
        }

        private static void DockPanelToExistingPanel(DockManager dockManager, DockingStyle dockStyle, DockPanel newPanel)
        {
            List<DockPanel> panels = new List<DockPanel>();

            foreach (DockPanel existingPanel in dockManager.Panels)
                panels.Add(existingPanel);

            foreach (DockPanel existingPanel in panels)
                if ((existingPanel.Dock == dockStyle) && (existingPanel != newPanel))
                {
                    existingPanel.DockAsTab(newPanel);
                    continue;
                }
        }

        private List<SpellChecker> spellCheckers = new List<SpellChecker>();

        private void CreateSpellChecker(Control parentContainer)
        {
            SpellChecker spellChecker = new SpellChecker();
            spellCheckers.Add(spellChecker);
            spellChecker.SpellCheckMode = SpellCheckMode.AsYouType;
            spellChecker.ParentContainer = parentContainer;
            spellChecker.CheckAsYouTypeOptions.CheckControlsInParentContainer = true;
        }
    }
}

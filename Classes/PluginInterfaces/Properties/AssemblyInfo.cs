﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PluginInterfaces")]
[assembly: AssemblyDescription("Technician Integrated Data Environment")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Victory Technologies, Inc.")]
[assembly: AssemblyProduct("Victory Technician Services")]
[assembly: AssemblyCopyright("Copyright © Victory Technologies, Inc. 2008-2010")]
[assembly: AssemblyTrademark("Technician Services(tm) Victory Technologies, Inc. 2008-2010")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2d4b465a-b9b2-4c2e-9333-2954dd709c35")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.0.4.5")]
[assembly: AssemblyFileVersion("3.0.4.5")]

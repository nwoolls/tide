﻿using System;
using System.Collections.Generic;
using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.Skins;

namespace tide.Skins
{
    class SkinMenuBuilder
    {
        private DefaultLookAndFeel defaultLookAndFeel;
        private BarSubItem skinMenuItem;

        public SkinMenuBuilder(BarSubItem skinMenuItem, DefaultLookAndFeel defaultLookAndFeel)
        {
            this.skinMenuItem = skinMenuItem;
            this.defaultLookAndFeel = defaultLookAndFeel;
            PopulateSkins();
        }

        private void PopulateSkins()
        {
            List<SkinContainer> skins = new List<SkinContainer>();
            foreach (SkinContainer skin in SkinManager.Default.Skins)
                skins.Add(skin);
            skins.Sort(CompareSkinName);
            PopulateSkinMenu(skins);
            OrganizeSkins();
        }

        private static int CompareSkinName(SkinContainer skin1, SkinContainer skin2)
        {
            return skin1.SkinName.CompareTo(skin2.SkinName);
        }

        private void PopulateSkinMenu(List<SkinContainer> skins)
        {
            foreach (SkinContainer skin in skins)
            {
                BarButtonItem item = new BarButtonItem
                {
                    Caption = skin.SkinName,
                    ButtonStyle = BarButtonStyle.Check,
                    GroupIndex = 123
                };
                item.ItemClick += skinItemClick;
                if (defaultLookAndFeel.LookAndFeel.SkinName.Equals(skin.SkinName))
                    item.Down = true;
                skinMenuItem.ItemLinks.Add(item);
            }
        }

        private void skinItemClick(object sender, ItemClickEventArgs e)
        {
            defaultLookAndFeel.LookAndFeel.SkinName = e.Item.Caption;
        }

        private void OrganizeSkins()
        {
            OrganizeSeasonSkins();
            OrganizeOffice2007Skins();
        }

        private void OrganizeSeasonSkins()
        {
            BarSubItem subItem = new BarSubItem { Caption = "Seasonal" };
            skinMenuItem.ItemLinks.Insert(0, subItem);
            OrganizeSkinsInArray(subItem, SkinDetails.SeasonalSkins);
        }

        private void OrganizeOffice2007Skins()
        {
            BarSubItem subItem = new BarSubItem { Caption = "Office 2007" };
            skinMenuItem.ItemLinks.Insert(0, subItem);
            OrganizeSkinsInArray(subItem, SkinDetails.OfficeSkins);
        }

        private void OrganizeSkinsInArray(BarSubItem newSubItem, string[] skinNames)
        {
            List<BarItemLink> links = new List<BarItemLink>();
            foreach (BarItemLink link in skinMenuItem.ItemLinks)
                foreach (string skinName in skinNames)
                    if (link.Caption.Equals(skinName))
                    {
                        newSubItem.ItemLinks.Add(link.Item);
                        links.Add(link);
                    }

            foreach (BarItemLink link in links)
                skinMenuItem.ItemLinks.Remove(link);
        }
    }
}

/* Copyright (c) 2009, Nathanial Woolls
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 * conditions and the following disclaimer in the documentation and/or other materials provided with 
 * the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RemObjects.Hydra.CrossPlatform;
using tide.PluginInterfaces;
using RemObjects.Hydra;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using System.Globalization;
using DevExpress.XtraSpellChecker;
using System.IO;
using DevExpress.XtraPrinting;
using tide.Utilities.Forms;
using tide.Utilities.Settings;
using tide.Controllers;
using tide.Skins;

namespace tide
{
    public partial class MainForm : RibbonForm, IHostApplication
    {
        private RibbonController ribbonController;
        private NavBarController navBarController;
        private StatusBarController statusBarController;
        private DockManagerController dockManagerController;
        private PluginPageController pluginPageController;

        private SkinMenuBuilder skinMenuBuilder;

        public MainForm()
        {
            InitializeComponent();

            DevExpress.UserSkins.OfficeSkins.Register();
            DevExpress.UserSkins.BonusSkins.Register();

            LoadPlugins();

            Text = String.Format("{0} {1}", BrandingProvider.ShortCompanyName(), 
                BrandingProvider.ApplicationName());
            Application.Idle += HandleApplicationIdle;

            ribbonController = new RibbonController(ribbonControl, pluginList);
            navBarController = new NavBarController(navBarControl, pluginList);
            statusBarController = new StatusBarController(ribbonStatusBar, pluginList);
            dockManagerController = new DockManagerController(dockManager, pluginList);
            pluginPageController = new PluginPageController(pluginTabControl, pluginList);

            BuildMenuItems();

            LoadSettings();

            skinMenuBuilder = new SkinMenuBuilder(skinsSubItem, lookAndFeel);
        }

        private List<IHYCrossPlatformPlugin> pluginList;

        #region IHostApplication Members

        public List<IHYCrossPlatformPlugin> Plugins()
        {
            return pluginList;
        }

        public void DisplayPlugin(IVisualPlugin plugin)
        {
            pluginPageController.DisplayPlugin(plugin);
        }

        public void DisplayDetailWindow(IMasterWindow parent, IDetailWindow detail)
        {
            pluginPageController.DisplayDetailWindow(parent, detail);
        }

        public void DisplayMasterDetails(IMasterWindow parent)
        {
            pluginPageController.DisplayMasterDetails(parent);
        }

        #endregion
        
        #region Event Handlers

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            using (new HourGlass())
            {
                SaveSettings();

                UnloadPlugins();
            }
        }

        private void otherExportButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportActivePluginToOther();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (new HourGlass())
                DeleteItemForActivePlugin();
        }

        private void printButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            PrintActivePlugin();
        }

        private void newItemMenu_Popup(object sender, EventArgs e)
        {
            BarSubItem menu = (BarSubItem)sender;
            EnablePluginNewItems(menu);
        }

        private void saveItemMenu_Popup(object sender, EventArgs e)
        {
            BarSubItem menu = (BarSubItem)sender;
            EnablePluginSaveItems(menu);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !BrowseWindowsAreOkayToClose(); 
        }

        private void optionsButton_Click(object sender, EventArgs e)
        {
            applicationMenu.HidePopup();
            using (OptionsForm form = new OptionsForm(moduleManager, this))
                form.ShowDialog(this);
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ribbonControl_ApplicationButtonDoubleClick(object sender, EventArgs e)
        {
            applicationMenu.HidePopup();
            Close();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (new HourGlass())
                NewItemForActivePlugin();
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (new HourGlass())
                SaveItemForActivePlugin();
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            PrintPreviewActivePlugin();
        }

        private void htmlExportButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportActivePluiginToHtml();
        }

        private void pdfExportButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportActivePluginToPdf();
        }

        private void xlsExportButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportActivePluginToXls();
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            QuickPrintActivePlugin();
        }

        #endregion

        #region Exporting Data To a File
        
        private void ExportActivePluiginToHtml()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IPrintableProvider)
            {
                string fileName = BrowseForFile("Webpages|*.html;*.htm");
                if (!string.IsNullOrEmpty(fileName) && Directory.Exists(Path.GetDirectoryName(fileName)))
                {
                    using (PrintableComponentLink link = new PrintableComponentLink(printingSystem) { 
                        Component = ((IPrintableProvider)activePlugin).Printable() })
                        link.CreateDocument();
                    printingSystem.ExportToHtml(fileName);
                }
            }
        }

        private void ExportActivePluginToPdf()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IPrintableProvider)
            {
                string fileName = BrowseForFile("Acrobat Documents|*.pdf");
                if (!string.IsNullOrEmpty(fileName) && Directory.Exists(Path.GetDirectoryName(fileName)))
                {
                    using (PrintableComponentLink link = new PrintableComponentLink(printingSystem) { 
                        Component = ((IPrintableProvider)activePlugin).Printable() })
                        link.CreateDocument();
                    printingSystem.ExportToPdf(fileName);
                }
            }
        }

        private void ExportActivePluginToXls()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IPrintableProvider)
            {
                string fileName = BrowseForFile("Excel Spreadsheets|*.xls");
                if (!string.IsNullOrEmpty(fileName) && Directory.Exists(Path.GetDirectoryName(fileName)))
                {
                    using (PrintableComponentLink link = new PrintableComponentLink(printingSystem) { 
                        Component = ((IPrintableProvider)activePlugin).Printable() })
                        link.CreateDocument();
                    printingSystem.ExportToXls(fileName);
                }
            }
        }

        private void ExportFileByExtension(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            switch (extension.ToUpper())
            {
                case ".CSV":
                    printingSystem.ExportToCsv(fileName);
                    break;
                case ".HTML":
                case ".HTM":
                    printingSystem.ExportToHtml(fileName);
                    break;
                case ".JPG":
                case ".JPEG":
                case ".GIF":
                case ".BMP":
                case ".PNG":
                    printingSystem.ExportToImage(fileName);
                    break;
                case ".MHT":
                    printingSystem.ExportToMht(fileName);
                    break;
                case ".PDF":
                    printingSystem.ExportToPdf(fileName);
                    break;
                case ".RTF":
                    printingSystem.ExportToRtf(fileName);
                    break;
                case ".XLS":
                case ".XLSX":
                    printingSystem.ExportToXls(fileName);
                    break;
                default:
                    printingSystem.ExportToText(fileName);
                    break;
            }
        }

        private void ExportPluginToFile(IVisualPlugin plugin, string fileName)
        {
            using (PrintableComponentLink link = new PrintableComponentLink(printingSystem) { 
                Component = ((IPrintableProvider)plugin).Printable() })
            {
                link.CreateDocument();
                ExportFileByExtension(fileName);
            }
        }

        private void ExportActivePluginToOther()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IPrintableProvider)
            {
                const string allFilters = "Comma Delimited Text|*.csv|Webpages|*.html;*.htm|Images|*.jpg;*.bmp;*.gif;*.png|MHTML|*.mht|Acrobat Documents|*.pdf|Richtext Documents|*.rtf|Text Files|*.txt|Excel Spreadsheets|*.xls";
                string fileName = BrowseForFile(allFilters);

                if (!string.IsNullOrEmpty(fileName) && Directory.Exists(Path.GetDirectoryName(fileName)))
                    ExportPluginToFile(activePlugin, fileName);
            }
        }

        #endregion

        #region Plugin UI Management
        
        private static void EnablePluginSaveItems(BarSubItem menu)
        {
            foreach (BarItemLink itemLink in menu.ItemLinks)
            {
                BarItem item = itemLink.Item;
                IMasterWindow browseWindow = (IMasterWindow)item.Tag;
                item.Enabled = browseWindow.SaveItemEnabled();
            }
        }

        private static void EnablePluginNewItems(BarSubItem menu)
        {
            foreach (BarItemLink itemLink in menu.ItemLinks)
            {
                BarItem item = itemLink.Item;
                IMasterWindow browseWindow = (IMasterWindow)item.Tag;
                item.Enabled = browseWindow.NewItemEnabled();
            }
        }

        private bool BrowseWindowsAreOkayToClose()
        {
            foreach (IHYCrossPlatformPlugin plugin in Plugins())
            {
                if (plugin is IMasterWindow && ((IMasterWindow)plugin).SaveItemEnabled())
                {
                    IMasterWindow browseWindow = (IMasterWindow)plugin;
                    switch (MessageBox.Show(string.Format("You have unsaved changes to the current {0}. Would you like to save your changes?",
                        browseWindow.ItemName().ToLower()),
                        "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.Yes:

                            bool success;

                            using (new HourGlass())
                                success = browseWindow.SaveItem();

                            if (!success)
                                return false;

                            break;
                        case DialogResult.Cancel: return false;
                    }
                }
            }
            return true;
        }

        private IVisualPlugin ActivePlugin()
        {
            return pluginPageController.ActivePlugin();
        }

        private void newItemClick(object sender, ItemClickEventArgs e)
        {
            using (new HourGlass())
                ((IMasterWindow)e.Item.Tag).NewItem(null);
        }

        private void saveItemClick(object sender, ItemClickEventArgs e)
        {
            using (new HourGlass())
                ((IMasterWindow)e.Item.Tag).SaveItem();
        }

        private void CreateNewMenuItemForPlugin(IHYCrossPlatformInterface plugin)
        {
            BarItem newItem = ribbonControl.Items.CreateButton(String.Format("New {0}", ((IMasterWindow)plugin).ItemName()));
            newItem.LargeGlyph = imageCollection1.Images[0];
            newItem.Tag = plugin;
            newItem.ItemClick += newItemClick;
            newItemMenu.ItemLinks.Add(newItem);
        }

        private void CreateSaveMenuItemForPlugin(IHYCrossPlatformInterface plugin)
        {
            BarItem newItem = ribbonControl.Items.CreateButton(String.Format("Save {0}", ((IMasterWindow)plugin).ItemName()));
            newItem.LargeGlyph = imageCollection1.Images[1];
            newItem.Tag = plugin;
            newItem.ItemClick += saveItemClick;
            saveItemMenu.ItemLinks.Add(newItem);
        }

        private void BuildMenuItems()
        {
            foreach (IHYCrossPlatformInterface plugin in Plugins())
                if (plugin is IMasterWindow)
                {
                    CreateNewMenuItemForPlugin(plugin);
                    CreateSaveMenuItemForPlugin(plugin);
                }
        }

        private void NewItemForActivePlugin()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IMasterWindow)
                ((IMasterWindow)activePlugin).NewItem(null);
        }

        private void DeleteItemForActivePlugin()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IMasterWindow)
                ((IMasterWindow)activePlugin).DeleteItem();
        }

        private void SaveItemForActivePlugin()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IMasterWindow)
            {
                IMasterWindow masterWindow = (IMasterWindow)activePlugin;
                if (BeforeSaveDetails(masterWindow))
                {
                    masterWindow.SaveItem();
                    AfterSaveDetails(masterWindow);
                }
            }
        }

        private bool BeforeSaveDetails(IMasterWindow masterWindow)
        {
            foreach (IHYCrossPlatformInterface plugin in Plugins())
                if (plugin is IDetailWindow)
                    if (!((IDetailWindow)plugin).BeforeSaveDetails(masterWindow))
                        return false;
            return true;
        }

        private void AfterSaveDetails(IMasterWindow masterWindow)
        {
            foreach (IHYCrossPlatformInterface plugin in Plugins())
                if (plugin is IDetailWindow)
                    if (!((IDetailWindow)plugin).AfterSaveDetails(masterWindow))
                        return;
        }

        private void QuickPrintActivePlugin()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IPrintableProvider)
                using (new PrintableComponentLink(printingSystem) { 
                    Component = ((IPrintableProvider)activePlugin).Printable() })
                    printingSystem.Print();
        }

        private void PrintActivePlugin()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IPrintableProvider)
                using (new PrintableComponentLink(printingSystem) { 
                    Component = ((IPrintableProvider)activePlugin).Printable() })
                    printingSystem.PrintDlg();
        }

        private void PrintPreviewActivePlugin()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            if (activePlugin is IPrintableProvider)
                using (PrintableComponentLink link = new PrintableComponentLink(printingSystem) { 
                    Component = ((IPrintableProvider)activePlugin).Printable() })
                    link.ShowPreview();
        }

        private void UpdateBrowseQuickAccessItems(IVisualPlugin activePlugin)
        {
            if (activePlugin is IMasterWindow)
            {
                IMasterWindow browseWindow = (IMasterWindow)activePlugin;
                newItemButton.Enabled = browseWindow.NewItemEnabled();
                saveItemButton.Enabled = browseWindow.SaveItemEnabled();
                deleteItemButton.Enabled = browseWindow.DeleteItemEnabled();

                newItemButton.Hint = string.Format("New {0}", browseWindow.ItemName());
                saveItemButton.Hint = string.Format("Save {0} Changes", browseWindow.ItemName());
                deleteItemButton.Hint = string.Format("Delete Selected {0}", browseWindow.ItemName());
            }
            else
            {
                newItemButton.Enabled = false;
                saveItemButton.Enabled = false;
                deleteItemButton.Enabled = false;
            }
        }

        private void UpdatePrintableQuickAccessItems(IVisualPlugin activePlugin)
        {
            if (activePlugin is IPrintableProvider)
            {
                IPrintableProvider provider = (IPrintableProvider)activePlugin;
                printButton.Enabled = provider.Printable() != null;
                quickPrintButton.Enabled = provider.Printable() != null;
                printPreviewButton.Enabled = provider.Printable() != null;
            }
            else
            {
                printButton.Enabled = false;
                quickPrintButton.Enabled = false;
                printPreviewButton.Enabled = false;
            }
        }

        private void UpdateQuickAccessItems()
        {
            IVisualPlugin activePlugin = ActivePlugin();
            UpdateBrowseQuickAccessItems(activePlugin);

            UpdatePrintableQuickAccessItems(activePlugin);
        }

        private void HandleApplicationIdle(object sender, EventArgs e)
        {
            UpdateQuickAccessItems();
        }

        #endregion

        #region Loading/Saving Settings

        private void LoadWindowState(SimpleSettings settings)
        {
            Top = settings.GetSetting("WindowState/Top", Top);
            Left = settings.GetSetting("WindowState/Left", Left);
            Height = settings.GetSetting("WindowState/Height", Height);
            Width = settings.GetSetting("WindowState/Width", Width);

            string stateString = settings.GetSetting("WindowState/WindowState", "Maximized");
            WindowState = (FormWindowState)Enum.Parse(typeof(FormWindowState), stateString, true);
        }

        private void LoadIspellDictionary(CultureInfo engCulture)
        {
            string path = String.Format(@"{0}\Dictionaries\English (United States)\", Application.StartupPath);
            SpellCheckerISpellDictionary dictionary = new SpellCheckerISpellDictionary(String.Format("{0}ispell.xlg", path),
                String.Format("{0}ispell.aff", path), engCulture);
            sharedDictionaryStorage.Dictionaries.Add(dictionary);
        }

        private void LoadCustomDictionary(CultureInfo engCulture)
        {
            string path = String.Format(@"{0}Dictionaries\", ApplicationSettings.SettingsPath);
            Directory.CreateDirectory(path);
            SpellCheckerCustomDictionary customDictionary = new SpellCheckerCustomDictionary(String.Format("{0}user.dic", path), engCulture);
            sharedDictionaryStorage.Dictionaries.Add(customDictionary);
        }        
        
        ApplicationSettings applicationSettings;
        private ApplicationSettings ApplicationSettings
        {
            get
            {
                if (applicationSettings == null)
                    applicationSettings = new ApplicationSettings(this);
                return applicationSettings;
            }
        }

        private void LoadDictionaries()
        {
            sharedDictionaryStorage.Dictionaries.Clear();

            CultureInfo engCulture = new CultureInfo("En-us");

            LoadIspellDictionary(engCulture);
            LoadCustomDictionary(engCulture);
        }

        private void LoadSettings()
        {
            SimpleSettings settings = new SimpleSettings(ApplicationSettings.SettingsFileName);

            LoadWindowState(settings);

            LoadDictionaries();
        }

        private void SaveWindowState(SimpleSettings settings)
        {
            settings.SetSetting("WindowState/WindowState", WindowState.ToString());
            if (WindowState != FormWindowState.Maximized)
            {
                settings.SetSetting("WindowState/Top", Top);
                settings.SetSetting("WindowState/Left", Left);
                settings.SetSetting("WindowState/Height", Height);
                settings.SetSetting("WindowState/Width", Width);
            }
        }

        private void SaveAppearance(SimpleSettings settings)
        {
            settings.SetSetting("Appearance/SkinName", lookAndFeel.LookAndFeel.SkinName);
        }

        private void SaveSettings()
        {
            SimpleSettings settings = new SimpleSettings(ApplicationSettings.SettingsFileName);

            SaveAppearance(settings);

            SaveWindowState(settings);
        }

        #endregion

        private string BrowseForFile(string filter)
        {
            saveFileDialog.FileName = "";
            saveFileDialog.Filter = filter;
            saveFileDialog.FilterIndex = 1;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                return saveFileDialog.FileName;
            return "";
        }

        private void CreatePluginInstances()
        {
            foreach (PluginDescriptor p in moduleManager.Plugins)
            {
                IHYCrossPlatformPlugin currentPlugin = moduleManager.CreateInstance(p);
                currentPlugin.Host = this;

                pluginList.Add(currentPlugin);
            }
        }

        private void LoadPluginModules()
        {
            // load all modules in the application's folder
            string[] files = Directory.GetFiles(String.Format("{0}\\Plugins",
                Path.GetDirectoryName(typeof(MainForm).Assembly.Location)), "*.dll");
            foreach (string file in files)
                moduleManager.LoadModule(file);
        }

        private void StartNonVisualPlugins()
        {
            if (Registered)
                foreach (IHYCrossPlatformPlugin plugin in Plugins())
                    if (plugin is IHYCrossPlatformNonVisualPlugin)
                        (plugin as IHYCrossPlatformNonVisualPlugin).Start();
        }

        private void InitializePlugins()
        {
            if (Registered)
                foreach (IHYCrossPlatformPlugin plugin in Plugins())
                    if (plugin is IInitializable)
                        (plugin as IInitializable).Initialize();
        }

        private void UninitializePlugins()
        {
            if (Registered)
                foreach (IHYCrossPlatformPlugin plugin in Plugins())
                    if (plugin is IInitializable)
                        (plugin as IInitializable).Uninitialize();
        }

        private void LoadAppearance(SimpleSettings settings)
        {
            lookAndFeel.LookAndFeel.SkinName = settings.GetSetting("Appearance/SkinName", BrandingProvider.DefaultSkinName());
        }

        private void LoadEarlySettings()
        {
            SimpleSettings settings = new SimpleSettings(ApplicationSettings.SettingsFileName);

            LoadAppearance(settings);
        }

        private void LoadPlugins()
        {
            pluginList = new List<IHYCrossPlatformPlugin>();

            LoadPluginModules();

            CreatePluginInstances();

            LoadEarlySettings();

            LoadRegistration();

            StartNonVisualPlugins();

            InitializePlugins();
        }

        private void UnloadPlugins()
        {
            UninitializePlugins();
            foreach (IHYCrossPlatformPlugin plugin in Plugins())
                if (plugin is IHYCrossPlatformNonVisualPlugin)
                    (plugin as IHYCrossPlatformNonVisualPlugin).Stop();
        }

        private void LoadRegistration()
        {
            Registered = !BrandingProvider.RequiresRegistration();
            foreach (IHYCrossPlatformPlugin plugin in Plugins())
                if (plugin is IRegistrationProvider)
                {
                    Registered = true;
                    if (!(plugin as IRegistrationProvider).LoadRegistration())
                    {
                        Registered = false;
                        Application.Exit();
                    }
                }
        }

        public bool Registered { get; set; }

        private IBrandingProvider brandingProvider;
        private IBrandingProvider BrandingProvider
        {
            get
            {
                if (brandingProvider == null)
                    foreach (IHYCrossPlatformPlugin plugin in Plugins())
                        if (plugin is IBrandingProvider)
                        {
                            brandingProvider = plugin as IBrandingProvider;
                            break;
                        }
                return brandingProvider;
            }
        }   
    }
}